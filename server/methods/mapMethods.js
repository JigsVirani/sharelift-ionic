

Meteor.methods({

  getMatrixTimeDistance: function(start, end){
    console.log("=== getMatrixTimeDistance called on server === ");

    // requestURL = 'http://jsonplaceholder.typicode.com/posts';
    let requestURL = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' + start.lat + ',' + start.long + '&destinations=' + end.lat + ',' + end.long + '&key=AIzaSyDBw8sOzxRaATf0IDCESW-FfqPlJHR8NBk';
    console.log("The request URL is: " + requestURL);

    //wrap the HTTP.get method to be a synchronous method (it won't continue until it's received the callback from Google )
    var synchronousHTTPGet  = Meteor.wrapAsync( HTTP.get ), //convertAsyncToSync is a function
      APIresponse = synchronousHTTPGet(requestURL, {} );  //pass the url and options to

        let responseInfo = {
          "originAddress": APIresponse.data.origin_addresses[0],
          "destinationAddress": APIresponse.data.destination_addresses[0],
          "duration": {
            "value": APIresponse.data.rows[0].elements[0].duration.value,
            "text": APIresponse.data.rows[0].elements[0].duration.text
          },
          "distance": {
            "value": APIresponse.data.rows[0].elements[0].distance.value,
            "text": APIresponse.data.rows[0].elements[0].distance.text
          }
        };

    return responseInfo;

  }



});
