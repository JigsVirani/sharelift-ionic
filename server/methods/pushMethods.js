Push.debug = true;  //adds verbosity

Push.allow({
    send: function(userId, notification) {
        return true; // Allow all users to send from console
        // return false;
    }
});

Meteor.methods({
    sendPushNotification: function(push) {
      console.log(push);
      var isValid = Match.test(push, pushNotifSchema);
			console.log(isValid);
			if(!isValid) {
				console.log("shit ain't right--pushNotifSchema violated");
				return;
			}

      var notifText = "";
      if(push.notifType == "denyLift") {
        notifText = "Sorry, " + push.userName + " is unable to give you a lift."
      } else if (push.notifType == "confirmLift") {
        notifText = push.userName + " has confirmed your request"
      } else if(push.notifType == "requestLift") {
        notifText = push.userName + " has requested your lift"
      } else {

      }

      Push.send({
        from: 'push', //required, but can be anything and doesn't matter actual text
        title: 'Sharelift',
        text: notifText,
        badge: 1,
        sound: 'default',
        query : {
          userId: push.userId
        }
      });

    },

    sendSinglePushNotification : function(pushData) {
      console.log("sendSinglePushNotification CALLED WITH FOLLOWING OBJECT: ");
      console.log(pushData);

      // console.log(pushData);
      // var isValid = Match.test(pushData, multiPushNotifSchema);
			// console.log(isValid);
			// if(!isValid) {
			// 	console.log("shit ain't right--pushNotifSchema violated");
			// 	return;
			// }

      Push.send({
        from: 'push', //required, but can be anything and doesn't matter actual text
        title: 'Sharelift',
        text: pushData.notifText,
        badge: 1,
        sound: 'default',
        query : {
          userId: pushData.userId  //notifications are sent to all IDs in sendTo array
        }
      });
      console.log("SINGLE PUSH SENT WITH TEXT:  " + pushData.notifText + " --- Sent to the following user: ");
      console.log(pushData.userId);


    },

    sendMultiplePushNotifications : function(pushData) {

      console.log("sendMultiplePushNotifications CALLED WITH FOLLOWING OBJECT: ");
      console.log(pushData);

      // console.log(pushData);
      // var isValid = Match.test(pushData, multiPushNotifSchema);
			// console.log(isValid);
			// if(!isValid) {
			// 	console.log("shit ain't right--pushNotifSchema violated");
			// 	return;
			// }

      Push.send({
        from: 'push', //required, but can be anything and doesn't matter actual text
        title: 'Sharelift',
        text: pushData.notifText,
        badge: 1,
        sound: 'default',
        query : {
          userId: {$in : pushData.userIds}  //notifications are sent to all IDs in sendTo array
        }
      });



      console.log("MULTIPLE PUSH NOTIFS SENT WITH TEXT:  " + pushData.notifText + " --- Sent to the following users: ");
      console.log(pushData.userIds);
    },

    testPushNotifications : function(){
      var facebookFriendIDs = Meteor.user().profile.facebookFriends;
      facebookFriendIDs.push(Meteor.user().services.facebook.id); //for testing purposes, the current user gets a notification too.

      var usersCursor = Meteor.users.find({"services.facebook.id": {$in : facebookFriendIDs}}, {"services.facebook.name": true});
      var sendTo = [];  //an array of Meteor IDs to send push notifications to

      var count = 0;
      usersCursor.forEach(function (user) {
        sendTo.push(user._id);
        count += 1;
      });


      var notifText = "Push notifications are back on ya'll!";

      Push.send({
        from: 'push', //required, but can be anything and doesn't matter actual text
        title: 'Sharelift',
        text: notifText,
        badge: 1,
        sound: 'default',
        query : {
          userId: {$in : sendTo}  //notifications are sent to all IDs in sendTo array
        }
      });

      console.log("Push notification sent to all FB friends with the following message: " + notifText);



    },

    sendFacebookFriendLiftAlert: function(notifText) {
      //should probably run it through a schema. Not going to do that for now

      console.log("sendFacebookFriendLiftAlert METHOD CALLED WITH NotifText EQUAL TO:");
      console.log(notifText);

      var facebookFriendIDs = Meteor.user().profile.facebookFriends;

      var usersCursor = Meteor.users.find({"services.facebook.id": {$in : facebookFriendIDs}}, {"services.facebook.name": true});
      var sendTo = [];  //an array of Meteor IDs to send push notifications to

      var count = 0;
      usersCursor.forEach(function (user) {
        sendTo.push(user._id);
        count += 1;
      });

      // var locationObject = Locations.findOne({locationId: liftAlert.destination});
      // var notifText = "Your Facebook friend " + liftAlert.driverName + " just posted a lift to " + locationObject.destinationName;

      Push.send({
        from: 'push', //required, but can be anything and doesn't matter actual text
        title: 'Sharelift',
        text: notifText,
        badge: 1,
        sound: 'default',
        query : {
          userId: {$in : sendTo}  //notifications are sent to all IDs in sendTo array
        }
      });

      console.log("NOTIFICATIONS TO FB FRIENDS CALLED WITH THE MESSAGE: " + notifText + " -- To the following users: ");
      console.log(sendTo);

    },

    notifyFriendsUsingSharelift: function(){

      var facebookFriendIDs = Meteor.user().profile.facebookFriends;

      var usersCursor = Meteor.users.find({"services.facebook.id": {$in : facebookFriendIDs}}, {"services.facebook.name": true});
      var sendTo = [];  //an array of Meteor IDs to send push notifications to

      var count = 0;
      usersCursor.forEach(function (user) {
        sendTo.push(user._id);
        count += 1;
      });

      var notifText = "Your friend " + Meteor.user().services.facebook.name + " is now using Sharelift!";

      Push.send({
        from: 'push', //required, but can be anything and doesn't matter actual text
        title: 'Sharelift',
        text: notifText,
        badge: 1,
        sound: 'default',
        query : {
          userId: {$in : sendTo}  //notifications are sent to all IDs in sendTo array
        }
      });

    },


    serverNotification: function(text,title) {
        var badge = 1
        Push.send({
            from: 'push',
            title: title,
            text: text,
            badge: 1,
            sound: 'airhorn.caf',
            payload: {
                title: title,
                text:text
            },
            query: {
                // this will send to all users
            }
        });
    },
    userNotification: function(text,title,userId) {
        var badge = 1
        Push.send({
            from: 'push',
            title: title,
            text: text,
            badge: badge,
            sound: 'airhorn.caf',
            payload: {
                title: title,
                historyId: result
            },
            query: {
                userId: userId //this will send to a specific Meteor.user()._id
            }
        });
    },
});
