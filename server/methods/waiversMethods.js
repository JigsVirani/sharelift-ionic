Meteor.methods({

  acceptPrivacyPolicy : function(){
    Meteor.users.update(Meteor.userId(), {
      $set:{
        "profile.hasAcceptedPrivacyPolicy" : true
      }
    });
    return true;
  },

  acceptTermsConditions : function(){
    Meteor.users.update(Meteor.userId(), {
      $set:{
        "profile.hasAcceptedTermsConditions" : true
      }
    });
    return true;
  },

//for testing purposes only. This sets all waiver values to false for the given user
  resetWaivers : function() {
    Meteor.users.update(Meteor.userId(), {
      $set:{
        "profile.hasAcceptedTermsConditions" : false,
        "profile.hasAcceptedPrivacyPolicy" : false,
      }
    });

  }

});
