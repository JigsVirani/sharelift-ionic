Meteor.methods({

  // exampleMethod: function() {
  //
  // },

  uploadProfilePhoto: function(data) {
    //check the data type?

    //The user has to be logged in
    if(!Meteor.userId()) {
      // throw new Meteor.Error('not-logged-in',
      //   'Must be logged in to update his picture.');
      console.log("user not logged in");
      return;
    }
    Meteor.users.update(Meteor.userId(), {
      $set: {
        "profile.profilePicture": data
      }
    });
  },

  uploadVehiclePhoto: function(data) {
    //check the data type?
    console.log(" == uploadVehiclePhoto called == ");
    console.log(data);

    //The user has to be logged in
    if(!Meteor.userId()) {
      // throw new Meteor.Error('not-logged-in',
      //   'Must be logged in to update his picture.');
      console.log("user not logged in");
      return;
    }
    Meteor.users.update(Meteor.userId(), {
      $set: {
        "profile.vehicle.vehiclePicture": data
      }
    });
  },

  addFacebookInfoToUser: function(data){
      //fake the data that will be pulled from fb
      Meteor.users.update(
        {_id : Meteor.userId()},
        {$set : {
          "services.facebook" : {
            email : data.email,
            first_name : data.firstName,
            last_name : data.lastName,
            name : data.fullName,
            gender : data.gender,
            locale : data.locale
          }
        }});
  }



});
