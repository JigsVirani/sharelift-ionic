Meteor.methods({
  confirmRideRequest: function(confirm) {
    console.log("------------- CONFIRM REQUEST -------------");
    console.log("The confirm object:");
    console.log(confirm);

// I20161215-00:31:12.813(-7)? { userId: 'FLFvwtDBWFEpFXyDv',
// I20161215-00:31:12.813(-7)?   userName: 'Samuel Kern',
// I20161215-00:31:12.813(-7)?   pickupDropoffLocation:
// I20161215-00:31:12.813(-7)?    { fullAddress: '301 W Story St, Bozeman, MT 59715, USA',
// I20161215-00:31:12.814(-7)?      vicinity: 'Bozeman',
// I20161215-00:31:12.814(-7)?      lat: 45.6745069,
// I20161215-00:31:12.814(-7)?      long: -111.04201139999998 },
// I20161215-00:31:12.814(-7)?   pickupTime: '',
// I20161215-00:31:12.814(-7)?   phone: '4063817970',
// I20161215-00:31:12.814(-7)?   googleRouteInfo:
// I20161215-00:31:12.814(-7)?    { originAddress: '300-398 W Story St, Bozeman, MT 59715, USA',
// I20161215-00:31:12.815(-7)?      destinationAddress: '15752-15766 Bridger Bowl Rd, Bozeman, MT 59715, USA',
// I20161215-00:31:12.815(-7)?      duration: { value: 1929, text: '32 mins' },
// I20161215-00:31:12.815(-7)?      distance: { value: 29932, text: '18.6 mi' } },
// I20161215-00:31:12.815(-7)?   '$$hashKey': 'object:1411',
// I20161215-00:31:12.815(-7)?   liftId: '2DTyrgRgHEH9NG97N' }

    // var isValid = Match.test(confirm, requestSchema);
    // console.log(isValid);
    // if(!isValid) {
    //   console.log("shit ain't right");
    //   return;
    // }

    //Remove all other lift requests that rider has put out


    var lift = Lifts.findOne({
      _id: confirm.liftId
    });
    if (Meteor.userId() != lift.driverId) {
      console.log("user tryna hack yo--confirming somebody else's");
      return false;
    }
    var rider;
    for (var i = 0; i < lift.potentialRiders.length; i++) {
      if (lift.potentialRiders[i].userId === confirm.userId) {
        rider = lift.potentialRiders[i];
      }
    }

    if (lift.confirmedRiders.length >= lift.seats) {
      return false;
    } else {

      let potentialLifts = Lifts.find({"potentialRiders.userId":confirm.userId}).fetch();

      for(let i = 0; i<potentialLifts.length; i++){
        let liftData = potentialLifts[i];

        //dismiss request notification from Driver
        let notifs = Notifications.find({liftId:liftData._id, riderId:confirm.userId}).fetch(); // Should only be one, but whatever.
        for(let i = 0; i<notifs.length; i++){
          if(notifs[i].liftId != confirm.liftId){ // Don't clear the notif asociated with this lift
              Notifications.update({_id : notifs[i]._id}, {$set: {dismissed: true}}); //Iterate through and dismiss them.
          }
        }

        Lifts.update({_id: liftData._id}, {$pull: {potentialRiders: {userId: confirm.userId} } }); //Pull rider from all lifts potential categories, including this one.

        //Give the user back a request
        Meteor.users.update(confirm.userId, {$inc: {"profile.numRequestsSent": -1,}});

      }

      //add the pickupDropOffConfirmed attribute to the rider object before inserting into the confirmedRiders array:
      rider.pickupDropOffConfirmed = false;
      console.log("rider object --> confirmedRiders array looks like this:");
      console.log(rider);

      Lifts.update({
        _id: confirm.liftId
      }, {
        $push: {
          confirmedRiders: rider
          //rider contains:
          // {userId : confirm.requestingUserId,
          // 	userName : confirm.userName,
          // 	pickupDropoffLocation : confirm.location,
          // 	pickupTime : "9:45 a.m."
          // }
        }
      });


      console.log(lift);
      var now = new Date();
      Notifications.insert({
        driverId: lift.driverId,
        riderId: confirm.userId,
        liftId: confirm.liftId,
        userName : lift.driverName,
        read: false,
        notifType: "confirmLift",
        date: now,
        dismissed:false
      });

      var push = {
        notifType : "confirmLift",
        userId : confirm.userId,
        userName : lift.driverName,
      }
      Meteor.call("sendPushNotification", push);

    }
    return true;

  },

  cancelConfirmedLift: function(cancel_lift){
    console.log("User tryna cancel lift");
    //console.log(cancel_lift);
    let lift = Lifts.findOne({_id:cancel_lift.liftId});
    if(Meteor.userId() == lift.driverId){
      console.log("User tryna hack -- Bailing on their own lift")
      return false;
    }
    let rider = Meteor.user();
    let rider_set = lift.confirmedRiders;
    console.log(rider_set);
    let new_rider_set = [];
    for(var x = 0; x < rider_set.length; x++){
      if(rider_set[x].userId == Meteor.userId()){
        rider_set.splice(x, 1);
      }
    };


    Lifts.update({
      _id: cancel_lift.liftId
    }, {
      $pull: {
        confirmedRiders: {
          userId:Meteor.userId()
        }
      }
    });

    console.log("New rider set:");
    console.log(lift.confirmedRiders);
    console.log(Meteor.user());
    //console.log(rider);

  },

  denyRideRequest: function(deny) {
    console.log("------------- DENY REQUEST -------------");

    // var isValid = Match.test(deny, requestSchema);
    // console.log(isValid);
    // if(!isValid) {
    //   console.log("shit ain't right");
    //   return;
    // }
    console.log("The deny object:");
    console.log(deny);


    var lift = Lifts.findOne({
      _id: deny.liftId
    });
    if (Meteor.userId() != lift.driverId) {
      console.log("user tryna hack yo, not their lift");
      return;
    }
    var rider;
    for (var i = 0; i < lift.potentialRiders.length; i++) {
      if (lift.potentialRiders[i].userId === deny.userId) {
        rider = lift.potentialRiders[i];
      }
    }
    console.log("The rider object:");
    console.log(rider);
    Lifts.update({
      _id: deny.liftId
    }, {
      $push: {
        deniedRiders: rider
      }
    });
    Lifts.update({
      _id: deny.liftId
    }, {
      $pull: {
        potentialRiders: {
          userId: deny.userId
        }
      }
    });
    Meteor.users.update(deny.userId, {$inc: {"profile.numRequestsSent": -1,}});
    var now = new Date();
    Notifications.insert({
      driverId: lift.driverId,
      riderId: deny.userId,
      liftId: deny.liftId,
      userName: lift.driverName,
      read: false,
      notifType: "denyLift",
      date: now,
      dismissed:false
    });

    // var push = {
    //   notifType : "denyLift",
    //   userId : deny.userId,
    //   userName : lift.driverName,
    // }

    var message = lift.driverName + " declined your request. Try requesting some more lifts!";
    var push = {
      userId : deny.userId,
      notifText : message,
    }
    Meteor.call("sendSinglePushNotification", push);
    // Meteor.call("sendPushNotification", push);

  },

  requestLift: function(request) {

    console.log(" ==== requestLift called. The request is: ==== ");
    console.log(request);

    // Looks like this:
    // request = {};
    // request.liftId = self.data._id;
    // request.userName = Meteor.user().services.facebook.name;
    // request.user = Meteor.user();
    // request.pickupDropoffLocation = findLiftInputService.getPickupDropoffLocation();
    // request.pickupTime = "";
    // request.driverId = self.data.driverId;
    // request.driverName = self.data.driverName;
    // request.phone = Meteor.user().profile.phone;


    // var isValid = Match.test(request, requestLiftSchema);
    // console.log(isValid);
    // if (!isValid) {
    //   console.log("shit ain't right--requestLiftSchema violated");
    //   return;
    // }

    //change to check if user's ID is anywhere in there && make so user can't request own lift, then insert
    lift = Lifts.findOne({
      _id: request.liftId
    });

    if (request.driverId === Meteor.userId()) {
      console.log("user tryna hack--can't request your own lift")
      return;
    }

    if (lift.confirmedRiders != null) {
      for (var i = 0; i < lift.confirmedRiders.length; i++) {
        if (lift.confirmedRiders[i].userId === Meteor.userId()) {
          console.log("user tryna hack--they already confirmed");
          return;
        }
      }
    }
    if (lift.deniedRiders != null) {
      for (var i = 0; i < lift.deniedRiders.length; i++) {
        if (lift.deniedRiders[i].userId === Meteor.userId()) {
          console.log("user tryna hack--they already been denied");
          return;
        }
      }
    }

    if (lift.potentialRiders != null) {
      for (var i = 0; i < lift.potentialRiders.length; i++) {
        if (lift.potentialRiders[i].userId === Meteor.userId()) {
          console.log("user tryna hack--they already requested");
          return;
        }
      }
    }


    //query the Google Matrix API to get the distance/time for the route
    let thisLocation = Locations.findOne({locationId: lift.destinationLocationId});


    let driverStartCoordinates = {};
    let pickupDropoffCoordinates = {};
    let endCoordinates = {};

    if(lift.liftDirection == 'toDestination'){
      // the driver's start location
      driverStartCoordinates.lat = lift.startEndLocation.lat;
      driverStartCoordinates.long = lift.startEndLocation.long;
      // the rider's pickup location
      pickupDropoffCoordinates.lat = request.pickupDropoffLocation.lat;
      pickupDropoffCoordinates.long = request.pickupDropoffLocation.long;
      // the destination location
      endCoordinates.lat = thisLocation.latitude;
      endCoordinates.long = thisLocation.longitude;
    }
    else{ //if fromDestination
      // the destination location (the starting point)
      driverStartCoordinates.lat = thisLocation.latitude;
      driverStartCoordinates.long = thisLocation.longitude;
      // the rider's dropoff location
      pickupDropoffCoordinates.lat = request.pickupDropoffLocation.lat;
      pickupDropoffCoordinates.long = request.pickupDropoffLocation.long;
      // the driver's end location
      endCoordinates.lat = lift.startEndLocation.lat;
      endCoordinates.long = lift.startEndLocation.long;
    }
    //the object that will be passed into the request
    let routeInfo =  {
      originToPickupDropoff : null,
      pickupDropoffToEnd : null
    }
    //calculate for routeInfo.originToPickupDropoff:
    Meteor.call('getMatrixTimeDistance', driverStartCoordinates, pickupDropoffCoordinates, function(error, result){
      if(error){
        alert("error!");
      }
      else{
        routeInfo.originToPickupDropoff = result;
        console.log(" -- getMatrixTimeDistance called. The result is: --");
        console.log(routeInfo.originToPickupDropoff);
        //do something with it...
      }
    });
    //calculate for routeInfo.pickupDropoffToEnd
    Meteor.call('getMatrixTimeDistance', pickupDropoffCoordinates, endCoordinates, function(error, result){
      if(error){
        alert("error!");
      }
      else{
        routeInfo.pickupDropoffToEnd = result;
        // console.log(" -- getMatrixTimeDistance called. The result is: --");
        // console.log(routeInfo.pickupDropoffToEnd);
        //do something with it...
      }
    });

    // console.log(" <<< routeInfo is: >>>");
    // console.log(routeInfo);

//For now, getting rid of a limit on the number of lift requests that can be out at one time
    // if (Meteor.user().profile.numRequestsSent >= 3) {
    //   console.log("user has already requested more than 3 lifts");
    //   return;
    // }
    Lifts.update({
      _id: request.liftId
    }, {
      $addToSet: {
        potentialRiders: {
          userId: Meteor.userId(),
          userName: request.userName,
          pickupDropoffLocation: request.pickupDropoffLocation,
          pickupTime: request.pickupTime,
          phone:request.phone,
          googleRouteInfo: routeInfo,
        }
      }
    });

    Meteor.users.update(Meteor.userId(), {
      $inc: {
        "profile.numRequestsSent": 1
      }
    });

    let notif = Notifications.insert({
      driverId: request.driverId,
      riderId: Meteor.userId(),
      liftId: request.liftId,
      userName : request.userName,
      read: false,
      notifType: "requestLift",
      date: new Date(),
      dismissed:false
    });

    console.log("NOTIF SENT OUT YO! " + notif);
    console.log("Sent to " + request.driverId);
    console.log(request);

    var push = {
      notifType : "requestLift",
      userId : request.driverId,
      userName : request.userName,
    }
    Meteor.call("sendPushNotification", push);

  },

  markNotifRead : function(notif) {
    Notifications.update({_id : notif}, {$set: {read: true}});
  },

  dismissNotif: function(notif){
    Notifications.update({_id : notif}, {$set: {dismissed: true}});
  },

  addProfilePhoneNumber: function(id, number){
    Meteor.users.update(id, {
      $set:{
        "profile.phone" : number
      }
    })

    // console.log(" -- addProfilePhoneNumber called. The user object is:");
    // console.log(Meteor.user());

  },

  updateCanContactUserField: function (boolean) {
    Meteor.users.update(Meteor.userId(), {
      $set: {
        "profile.allowContact" : boolean
      }
    })

    // console.log(" -- updateCanContactUserField called. The user object is:");
    // console.log(Meteor.user());

  },

  updateProfilePreferences: function(preferences){
    Meteor.users.update(Meteor.userId(), {
      $set: {
        "profile.settings.defaultDestination" : preferences.destinationId,
        "profile.settings.defaultRole" : preferences.defaultRole,
        "profile.hasOnboarded" : true
      }
    })
  },

  updateProfileAddress : function(id, address){
    Meteor.users.update(id, {
      $set: {
        "profile.address" : address
      }
    })
  },

  updateProfileVehicle : function(id, vehicle){
    Meteor.users.update(id, {
      $set : {
        "profile.vehicle.make" : vehicle.make,
        "profile.vehicle.model" : vehicle.model,
        "profile.vehicle.year" : vehicle.year,
        "profile.vehicle.seats" : vehicle.seats,
        "profile.vehicle.MPG" : vehicle.mpg

      }
    })
  },


  // resets the user's number of notifications for a specific day, liftType (toDestination or fromDestination), and destination
  resetLiftRequests: function(departureTime, liftType, destinationLocationId) {
    // ADD LOGIC HERE....

    //determine the day from the departure time (we need a filter for this)

    //search for lifts the user has requested that have the same parameters.
    //Count the number that are returned, and subtract that number from the user's total number of notifications

    //  "profile.numRequestsSent": X, //Setting number of requests to X


  },

  submitRating : function(rating) {

  	console.log(rating);
  	var isValid = Match.test(rating, ratingSchema);
  	if(!isValid) {
  	   console.log("shit ain't right, did not validate with ratingSchema");
  		return;
  	}

  	if(Ratings.findOne({liftId : rating.liftId, driverId : Meteor.userId(), riderId : rating.userId})) {
  		//user has already rated this lift
  		console.log("hacker no hacking, user has already rated");
  		return;
  	}

		Ratings.insert({
      ratingType : rating.ratingType, //means driver has rated rider
      liftId : rating.liftId,
      driverId : rating.driverId,
      riderId : rating.riderId,
      safe : rating.rating1,
      punctual : rating.rating2,
      friendly : rating.rating3,
      insertTime : new Date()
    });
		console.log("new rating inserted");
		return;
  },

  cancelRideRequest: function(liftData){

    //dismiss request notification from Driver
    let notifs = Notifications.find({liftId:liftData._id, riderId:Meteor.userId()}).fetch(); // Should only be one, but whatever.
    for(let i = 0; i<notifs.length; i++){
      Notifications.update({_id : notifs[i]._id}, {$set: {dismissed: true}}); //Iterate through and dismiss them.
    }

    //Lifts.update(liftData._id, {$pull: {potentialRiders: {userId: Meteor.userId()}}});
    Lifts.update({_id: liftData._id}, {$pull: {potentialRiders: {userId: Meteor.userId()} } });

    //Give the user back a request
    Meteor.users.update(Meteor.userId(), {$inc: {"profile.numRequestsSent": -1,}});
  },

  deleteLift : function(liftData) {
    //Some security measures
    if(!Meteor.userId()) {
      console.log("user not logged in");
      return;
    }

    //Gather all notifications related to the lift and dismiss them.
    let notifs = Notifications.find({liftId:liftData._id}).fetch(); // Gather all of the related notifications here
    for(let i = 0; i<notifs.length; i++){
      Notifications.update({_id : notifs[i]._id}, {$set: {dismissed: true}}); //Iterate through and dismiss them.
    }

    // if(!Meteor.userId() == liftData.driverId){
    //   console.log("User tryin a hack! Can't delete a lift if you're not the driver");
    //   return;
    // }

    // if(!Roles.userIsInRole(Meteor.userId(), ['driver'])) {
    //   console.log("User ain't a driver, so they can't delete a lift.");
    //   return;
    // }


    //var usersCursor = Meteor.users.find({"services.facebook.id": {$in : facebookFriendIDs}}, {"services.facebook.name": true});
    var confirmedRiderIDs = [];  //an array of Meteor IDs to send push notifications to
    var potentialRidersIDs = [];

    var now = new Date();
    var count = 0;

    var destination = Locations.findOne({locationId: liftData.destinationLocationId});
    var resortName = destination.destinationName;

    var notifText1 = liftData.driverName + " has canceled your lift to " + resortName;
    var notifText2 = "The lift you requested from " + liftData.driverName + " to " + resortName + " was canceled. Send some more requests!";

    liftData.confirmedRiders.forEach(function (riderData) {
      console.log("A confirmed rider:");
      console.log(riderData);
      confirmedRiderIDs.push(riderData.userId); //add each riderId to the confirmedRiderIDs array
      //Add a notification to the notifications collection for each passenger:
      Notifications.insert({
        driverId: liftData.driverId,
        riderId: riderData.userId,
        liftId: liftData._id,
        userName: riderData.userName,
        read: false,
        notifType: "deletedLift",
        date: now,
        notifText: notifText1,
        dismissed:false
      });
      count += 1;
    });


    var count = 0;
    liftData.potentialRiders.forEach(function (riderData) {
      console.log("A potential rider:");
      console.log(riderData);
      potentialRidersIDs.push(riderData.userId);
      //Add a notification to the notifications collection for each passenger:
      Notifications.insert({
        driverId: liftData.driverId,
        riderId: riderData.userId,
        liftId: liftData._id,
        userName: riderData.userName,
        read: false,
        notifType: "deletedLift",
        date: now,
        notifText: notifText2,
        dismissed:false
      });

      //Update each rider profile to decrease the number of requests sent by 1
      Meteor.users.update(riderData.userId, {$inc: {"profile.numRequestsSent": -1,}});
      count += 1;
    });

    //PUSH NOTIFICATIONS:
    //Send push notifs to confirmed riders:
    var pushData1 = {
      userIds : confirmedRiderIDs,
      notifText : notifText1,
    }
    Meteor.call("sendMultiplePushNotifications", pushData1);

    //Send push notifs to potential riders:
    var pushData2 = {
      userIds : potentialRidersIDs,
      notifText : notifText2,
    }
    Meteor.call("sendMultiplePushNotifications", pushData2);

    var counter = 0;
    liftData.confirmedRiders.forEach(function (riderData){
      console.log("confirmed rider " + counter + ":");
      console.log(Meteor.users.findOne(riderData.userId));
      counter += 1;
    });

    // console.log("Potential rider objects before execution:");
    var counter = 0;
    liftData.potentialRiders.forEach(function (riderData){
      console.log("potential rider " + counter + ":");
      console.log(Meteor.users.findOne(riderData.userId));
      counter += 1;
    });

    //remove the lift from the lifts collection
    Lifts.remove(liftData._id);

    console.log("The lift has been deleted and passengers notified!");
    //still need to determine how to display in-app notifications... Is adding to the Notifications collection enough?
  },



  cancelRider : function(liftData) {  //removes the user from the confirmed riders array, resets the user's requests for that time/destination/direction, and sends out notifications to driver

    //Some security measures
    if(!Meteor.userId()) {
      console.log("user not logged in");
      return;
    }
    // if(liftData.confirmedRiders.indexOf(Meteor.userId()) == -1){
    //   console.log("user tryn' a hack! Can't cancel a lift if you're not confirmed in it!");
    //   return;
    // }

    //Grab the notifications associated with the lift in which you are the rider
    let notifs = Notifications.find({liftId:liftData._id, riderId:Meteor.userId()}).fetch(); // Grab notifs related to the lift where you're the rider
    for(let i = 0; i<notifs.length; i++){
      Notifications.update({_id : notifs[i]._id}, {$set: {dismissed: true}}); //Iterate through and dismiss them.
    }

    // Reset the number of lift requests the user has for searches of the same lift day, liftType, and destination:
    Meteor.call("resetLiftRequests", liftData.departureTime, liftData.liftType, liftData.destinationLocationId);

    //Remove rider from confirmedRiders array:
    Lifts.update(liftData._id, {$pull: {confirmedRiders: {userId: Meteor.userId()}}});

    //Get the name of the destination
    var destination = Locations.findOne({locationId: liftData.destinationLocationId});
    var resortName = destination.destinationName;

    //ADD IN-APP NOTIFICATIONS & SEND PUSH NOTIFS:
    var notifToDriver = Meteor.user().services.facebook.first_name + " just canceled their lift with you to " + resortName;
    var notifToUser = "You canceled your lift with " + liftData.driverName;

    var now = new Date();
    //For driver:
    Notifications.insert({
      driverId: liftData.driverId,
      riderId: Meteor.userId(),
      liftId: liftData._id,
      userName: Meteor.user().services.facebook.name,
      read: false,
      notifType: "canceledRider", //a new notifType - needs to be handled differently on front end
      notifText: notifToDriver,
      date: now,
      dismissed:false
    });
    //For passenger who canceled (user):
    Notifications.insert({
      driverId: liftData.driverId,
      riderId: Meteor.userId(),
      liftId: liftData._id,
      userName: Meteor.user().services.facebook.name,
      read: false,
      notifType: "cancelConfirmation",  //a new notifType - needs to be handled differently on front end
      date: now,
      notifText: notifToUser, //use this text in the notification.
      dismissed:false
    });

    //Push notification for driver
    var driverPush = {
      userId : liftData.driverId,
      notifText : notifToDriver,
    }
    Meteor.call("sendSinglePushNotification", driverPush);

    //still need to determine how to display in-app notifications... Is adding to the Notifications collection enough?
  },

 setCurrentLocation: function(position) {
   Meteor.users.update(Meteor.userId(), {
     $set:{
       "profile.currentLocation" : position
     }
   });
 }

});
