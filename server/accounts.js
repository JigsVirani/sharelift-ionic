Accounts.onCreateUser(function(options, user) {
	now = new Date();
	profile = {
    // "facebookID" : 000000000000000, //for testing, but doesn't really do anything helpful
		"address" : "",
    "birthDate" : now, //obviously change
		"age" : null,
		"phone": "",
		"skierOrBoarder" : "",
    "vehicle" : {
            "make" : "",
            "model" : "",
            "year" :"",
            "seats" :"",
						"color" : "",
						"MPG" : ""
    },
		"settings" : {
			"defaultDestination" : 1,
			"defaultPickupLocation": null,
			"defaultDepartureTime" : null,
			"preferredAgeRangeMin" : 18,
			"preferredAgeRangeMax" : 25,
			"defaultCity" : "",
			"defaultRole" : "rider"

		},
		"numRequestsSent" : 0,
		"hasOnboarded" : false,
		"hasAcceptedTermsConditions" : false,
		"hasAcceptedPrivacyPolicy" : false,
		"currentLocation" : null
	}

	user.profile = profile;

//Right now, we are making EVERY user a driver when their account is created:
	Roles.addUsersToRoles(user._id, ["driver"]);

	return user;
});
