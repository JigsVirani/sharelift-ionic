angular
  .module('Sharelift')
  .config(config);

function config($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider) {

  uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyBjUTnBj0wLepey0FipdJgbo0Q7s_2fvHs',
      v: '3.20',
      libraries: 'weather,geometry,visualization,places'
  });


  $stateProvider
    .state('Main', {
      url: "/index",
      abstract: true,
      templateUrl: "client/templates/main.html",
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        },
        getUser() {
          return Meteor.user();
        },
        getLifts() {
          return Meteor.subscribe('getOpenLifts');
        },
      }
    })
    .state('Main.ride', {
      url: "/ride",
      views: {
        'ride-tab': {
          templateUrl: "client/templates/tabs/ride.html",
          controller: 'MainCtrl as main'
        }
      }
    })
    .state('Main.myLifts', {
      url: "/myLifts",
      views: {
        'my-lifts-tab': {
          templateUrl: "client/templates/tabs/myLifts.html",
          controller: 'MainCtrl as main'
        }
      }
    })
    .state('Main.drive', {
      url: "/drive",
      views: {
        'drive-tab': {
          templateUrl: "client/templates/tabs/drive.html",
          controller: 'MainCtrl as main'
        }
      }
    })


    // .state('Main', {
    //   url: '/index',
    //   abstract: false,
    //   templateUrl: 'client/templates/main.html',
    //   controller: 'MainCtrl as main',
    //   resolve: {
    //     "currentUser": function($meteor) {  //check that the user is logged in
    //       return $meteor.requireUser();
    //     },
    //     getUser() {
    //       return Meteor.user();
    //     },
    //     getLifts() {
    //       return Meteor.subscribe('getOpenLifts');
    //     },
    //   }
    // })

// State changes from RIDE TAB

    .state('Main.lift', {
      url: '/lift/:liftId',
      views: {
        'ride-tab': {
          templateUrl: 'client/templates/liftView.html',
          controller: 'LiftViewCtrl as lift'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        liftId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.liftId);
          return $stateParams.liftId;
        }]
      }
    })
    .state('Main.driverLift', {
      url: '/driverLift/:liftId',
      views: {
        'ride-tab': {
          templateUrl: 'client/templates/liftViewDriver.html',
          controller: 'LiftViewCtrl as lift'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        liftId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.liftId);
          return $stateParams.liftId;
        }]
      }
    })

// State changes from MY LIFTS TAB
    .state('Main.myLift', {
      url: '/myLift/:liftId',
      views: {
        'my-lifts-tab': {
          templateUrl: 'client/templates/liftView.html',
          controller: 'LiftViewCtrl as lift'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        liftId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.liftId);
          return $stateParams.liftId;
        }]
      }
    })
    .state('Main.myLiftDriver', {
      url: '/myLiftDriver/:liftId',
      views: {
        'my-lifts-tab': {
          templateUrl: 'client/templates/liftViewDriver.html',
          controller: 'LiftViewCtrl as lift'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        liftId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.liftId);
          return $stateParams.liftId;
        }]
      }
    })

    // .state('notifications', {
    //   url: '/notifications',
    //   templateUrl: 'client/templates/partials/notifications.html',
    //   controller: 'MainCtrl as main',
    //   resolve: {
    //     "currentUser": function($meteor) { //check that the user is logged in
    //       return $meteor.requireUser();
    //     },
    //     getProfile($stateParams) {
    //       return Meteor.subscribe('getPublicProfileData', $stateParams.userId);
    //     }
    //   }
    // })


    .state('profile', {
      url: '/profile/:userId',
      templateUrl: 'client/templates/publicProfile.html',
      controller: 'ProfileCtrl as profile',
      resolve: {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        getProfile($stateParams) {
          return Meteor.subscribe('getPublicProfileData', $stateParams.userId);
        }
      }
    })
      // All login screens
    .state('login', {
      url: '/login',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/login.html',
      controller : 'LoginCtrl'
    })
    .state('forgotPassword', {
      url: '/forgotPassword',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/forgotPassword.html',
      controller : 'LoginCtrl'
    })
    .state('createAccount', {
      url: '/createAccount',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/createAccount.html',
      controller : 'LoginCtrl'
    })
    .state('addFBInfo', {
      url: '/addFBInfo',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/addFBInfo.html',
      controller : 'LoginCtrl',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    })
    .state('inputProfileSettings', {
      url: '/inputProfileSettings',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/inputProfileSettings.html',
      controller : 'LoginCtrl',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    })
    .state('phoneInput', {
      url: '/phone-input',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/phoneInput.html',
      controller : 'LoginCtrl',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    })
    .state('driverSignup', {
      url: '/driverSignup',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/driverSignup.html',
      controller : 'DriverSignupCtrl',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    });



  $urlRouterProvider.otherwise('/index/myLifts');
}
