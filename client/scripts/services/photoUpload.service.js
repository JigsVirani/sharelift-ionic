angular
  .module('Sharelift')
  .service('photoUploadService', photoUploadService);

function photoUploadService($meteor){

  this.uploadVehiclePhoto = function(){
    $meteor.getPicture().then(function(data){
      console.log("-- uploadVehiclePhoto called --");

      console.log("converting to a file:");
      let file = dataURItoBlob(data);

      //Code for uploading a blob to Amazon S3
      const uploader = new Slingshot.Upload( "uploadToAmazonS3" );
      uploader.send( file, ( error, url ) => {
        if ( error ) {

          console.log("Error!");
          // return null;
        } else {
          console.log("_uploadFileToAmazon successful. The url is:");
          console.log(url);

          if(url){  //if the image was successfully uploaded and the url returned
            Meteor.call('uploadVehiclePhoto', url); // save the URL
          }
          else{
            alert("Something went wrong");
          }
        }
      });

    });
  }

// takes the data URI from the camera and turns it into a blob!
function dataURItoBlob(dataURI) {
  console.log("dataURItoBlob called");
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
    for(var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
}



}
