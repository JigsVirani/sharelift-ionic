angular.module('Sharelift')
.controller('PopupCtrl',['$scope', '$ionicPopup', '$timeout', '$controller', function($scope, $ionicPopup, $timeout, $controller) {


  // This is a service used for popups. If you need to create a new one, make a new template
  // This is bascially a popup API.

  this.showPopup = function(title, subTitle, template, buttons){
    $scope.showPopup(title, subTitle, template, buttons);
  }

  this.showConfirm = function(title, content, callback, yesText, noText){
    if(!yesText){
      yesText = "Ok";
    }
    if(!noText){
      noText = "Cancel";
    }
    return($scope.showConfirm(title, content, callback, yesText, noText));
  }

  this.showAlert = function(title, body, callback){
    return($scope.showAlert(title, body, callback));
  }

  this.showInputPopup = function(title, subTitle, callback){
    return($scope.showInputPopup(title, subTitle,callback));
  }

// Triggered on a button click, or some other target
$scope.showPopup = function(title, subTitle, template, buttons) {
  $scope.data = {};

  // An elaborate, custom popup
  var myPopup = $ionicPopup.show({
    template: template,
    title: title,
    subTitle: subTitle,
    scope: $scope,
    buttons: buttons
  });

  myPopup.then(function(res) {
    console.log('Tapped!', res);
  });
 };

 $scope.showInputPopup = function(title, subTitle, callback){
   $scope.data = {};
   var myPopup = $ionicPopup.show({
     template: "<input type='text' ng-model='data.inputPopupData'/>",
     title: title,
     subTitle: subTitle,
     scope: $scope,
     buttons: [
       { text:"Cancel" },
       {
         text:"Save",
         type: 'button-dark-blue',
         onTap: function(e){
           console.log($scope.data);
           if(!$scope.data.inputPopupData){
             e.preventDefault();
           }
           else{
             callback($scope.data.inputPopupData);
           }
         }
       }
     ]
   });
 }

 // A confirm dialog
 $scope.showConfirm = function(title, content, callback, yesText, noText) {
   var confirmPopup = $ionicPopup.confirm({
     title: title,
     template: content,
     buttons:[
       {
         text:noText,
         onTap: function(){
           callback(0);
         }
       },
       {
         text:yesText,
         type:'button-dark-blue',
         onTap:function(){
           callback(1);
         }
       }
     ]
   });
 };

 // An alert dialog
 $scope.showAlert = function(title, body, callback) {
   var alertPopup = $ionicPopup.alert({
     title: title,
     template: body,
     buttons:[
       {
         text:"OK",
         type:'button-dark-blue'
       }
     ]
   });

   alertPopup.then(function(res) {
     callback(1);
     return 1;
   });
 };
}]);
