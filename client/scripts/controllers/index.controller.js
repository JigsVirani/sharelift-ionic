angular
  .module('Sharelift')
  .controller('IndexCtrl', IndexCtrl);

function IndexCtrl ($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $ionicNavBarDelegate, $interval, $rootScope, $location, navBarService) {
  let reactiveContext = $reactive(this).attach($scope);
  var popupController = $controller('PopupCtrl', {$scope: $scope});

  angular.expandControllerIndexPayment($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $location, popupController);
  angular.expandControllerIndexWaivers($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $location, popupController);



  $rootScope.navBarService = navBarService;


  //Notification subscriptions
  // this.subscribe('getRiderNotificationsConfirm');
  // this.subscribe('getRiderNotificationsDeny');
  this.subscribe('getAllRiderNotifications');
  this.subscribe('getDriverNotifications');

  //Lift Subscriptions
  this.subscribe('liftsWhereRider');  //gets data for lifts where user is a rider

  $ionicNavBarDelegate.align(["center"]);
  $scope.unreadNotifs = false;
  $scope.unreadNotifTotal = 0;

  $scope.isInActiveLift = false;

  // returns a string with the connection status. Options are connected (the connection is up and running), connecting (disconnected and trying to open a new connection), failed (permanently failed to connect; e.g., the client and server support different versions of DDP), waiting (failed to connect and waiting to try to reconnect) and offline (user has disconnected the connection).
  $scope.getConnectionStatus = function(){
    var statusObject = Meteor.status();
    return statusObject.status;
  };

  // I think we can go with this one since it's reactive: (If you're right, delete the getConnectionStatus function above)
  $scope.connectionStatus = Meteor.status().status;

//Observer for the notifications data model
  let driverNotif = Notifications.find({driverId:Meteor.userId()});
  let riderNotif = Notifications.find({riderId:Meteor.userId()});

  var handleDriver = driverNotif.observeChanges({
    added: function(){
      $scope.notifCheck();

    }
  });

  var handleRider = riderNotif.observeChanges({
    added:function(){
      $scope.notifCheck();

    }
  })


//Function to update notif variables
  $scope.notifCheck = function(){
    let id = Meteor.userId();
    let driverNotifs = Notifications.find({driverId:id}).fetch();
    let riderNotifs = Notifications.find({riderId:id}).fetch();
    let notifs = driverNotifs.concat(riderNotifs);
    let unreadTotal = 0;
    if((notifs.length) > 0){
      for(let i = 0; i<notifs.length; i++){
        if(notifs[i].read == false && notifs[i].dismissed == false){
          unreadTotal++;
        }
      }
    }
    if(unreadTotal > 0){
      $scope.unreadNotifs = true;
    }
    else{
      $scope.unreadNotifs = false;
    }
    $scope.unreadNotifTotal = unreadTotal;
  }

// This function takes the user to the notifs page. It is used on the notification icon.
  $scope.goToNotifs = function(){
    $scope.modal1.show();
  }

  $scope.goToProfile = function(){
    $scope.modal2.show();
  }

  $scope.closeNotifModal = function() {
    $scope.modal1.hide();
  };

  $scope.closeProfileModal = function() {
    $scope.modal2.hide();
  };

  $ionicModal.fromTemplateUrl('client/templates/notifications.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.modal1 = modal;
  });

  $ionicModal.fromTemplateUrl('client/templates/profile.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.modal2 = modal;
  });

  //checks for notifications every second
  $interval($scope.notifCheck, 1000);


  //////////////////////////////////////
  ///////////// APP STATE //////////////
  //////////////////////////////////////

  //runs every 5 seconds - use this to trigger state changes in the database.
  //Check for lifts that should change to active, put geo-fence triggers here, etc.
  $scope.calculateAppState = function() {
    if(Meteor.user()){  //If the user is logged in

      //query a lift the user hasn't paid for
      var unpaidLift = Lifts.findOne({isComplete: true, driverId: {$not: Meteor.userId()}, payments : {$not : {$elemMatch: {fromUserId: Meteor.userId()}}}});
      // console.log("Here is a lift this user has NOT paid for:");
      // console.log(unpaidLift);

      if(unpaidLift){ //if there is a lift the user hasn't paid for
        $scope.openPaymentModal(unpaidLift);
      }

    } else {  //if the user is not logged in
      $scope.isInActiveLift = false;
    }

    // console.log("-- Connection status: --");
    // console.log($scope.getConnectionStatus());

  }

  // Re-calculates the app's state every 5 seconds
  $interval($scope.calculateAppState, 5000);


}
