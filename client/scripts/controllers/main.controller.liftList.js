//connects this controller with main.controller.js
angular.expandControllerMainLiftList = function($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController, findLiftInputService, liftStateService){

  // Functions for destination location selection

  //$location.path("lift/"+liftId); Syntax for redirecting to a lift

  //a Location object. Initially set to the user's favorite resort
  $scope.selectedResort;
  // $scope.pickupDropOffAddress;
  $scope.pickupDropOffAddress = findLiftInputService.fullAddress;  //initially set to the full address string of the address held in the findLiftInputService, if the user has a saved address

  $scope.liftDirectionFilter = "toDestination";

  //this is kinda stupid. Can't you just do this by seeing if selectedResort has been assigned a value?
  $scope.hasSelectedResort = function(){
    if($scope.selectedResort){
      return true;
    }
    else{
      return false;
    }
  }

  // Functions for pickup location selection
  let _pickup;

  $scope.pickupInput = {
    location: function(newLocation){
      return arguments.length ? (_pickup = newLocation) :  _pickup;
    }
  };

  $scope.dateInput = new Date();



  $scope.goToLiftView = function(id){
    console.log("goToLiftView called! The current findLiftInputService object is:");
    console.log(findLiftInputService);
    if(!Meteor.userId()){
      popupController.showAlert("Heads up!", "You aren't logged in. Please do that before viewing lifts.", function(){
        $scope.activeSlide = 0;
      });
    }
    else if(!findLiftInputService.pickupDropoffLocation){
      popupController.showAlert("Oh snap!", "You haven't provided a pickup location! Go ahead and fill that in first.", function(){});
    }
    else{
      $location.path("index/lift/" + id);
    }
  }

  $scope.goToLiftViewDriver = function(id){
    if(!Meteor.userId()){
      popupController.showAlert("Heads up!", "You aren't logged in. Please do that before viewing lifts.", function(){
        $scope.activeSlide = 0;
      });
    }
    else if(!Meteor.user().profile.address){
      popupController.showAlert("Oh snap!", "You haven't provided a pickup location! Go ahead and fill that in first.", function(){});
    }
    else{
      $location.path("index/driverlift/" + id);
    }
  }


// Move this to the myLifts controller when you create it
  $scope.goToLiftViewFromMyLifts = function(id){
    if(!Meteor.userId()){
      popupController.showAlert("Heads up!", "You aren't logged in. Please do that before viewing lifts.", function(){
        $scope.activeSlide = 0;
      });
    }
    else{
      $location.path("index/myLift/" + id);
    }
  }
  // Move this to the myLifts controller when you create it
    $scope.goToLiftViewDriverFromMyLifts = function(id){
      if(!Meteor.userId()){
        popupController.showAlert("Heads up!", "You aren't logged in. Please do that before viewing lifts.", function(){
          $scope.activeSlide = 0;
        });
      }
      else{
        $location.path("index/myLiftDriver/" + id);
      }
    }

  // $scope.savePickup = function(){
  //   this.pickupInput.location(document.getElementById('pickupLocationInput').value);
  //   /* This is mapbox stuff. It's important. I should move it elsewhere.
  //   Mapbox.load();
  //   function checkLoaded(){
  //     if(Mapbox.loaded() == false){
  //       setTimeout(function() {
  //           checkLoaded();
  //       }, 500);
  //     }
  //     else{
  //       L.mapbox.accessToken = 'pk.eyJ1IjoibnJvYmVydHVzIiwiYSI6ImNpbWQwMnczMDAwNDl2Mm02dWY1emhuNjMifQ._ua-9zvspJGn39ErOK0l9Q';
  //       L.mapbox.map('testMap', 'mapbox.streets').addControl(L.mapbox.geocoderControl('mapbox.places', {autocomplete: true}));
  //     }
  //   }
  //   checkLoaded();
  //   */
  //     if(!Meteor.userId()){
  //       popupController.showAlert("Heads up!", "You're not logged in. You can browse, but you won't be able to request any of these lifts.", function(){});
  //     }
  //     Meteor.call('updateProfileAddress', Meteor.userId(), _pickup);
  //
  // }

  $scope.updatePickupInput = function(){
    let user = Meteor.users.findOne({_id:Meteor.userId()});
    if(user && user.profile && user.profile.address){
      _pickup = user.profile.address;
      $scope.pickupInput.location();
    }
  }

  $scope.getLiftDay = function(lift){
    var weekday = new Array(7);
    weekday[0]=  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    let ret;
    if(lift && lift.date){
      let liftDate = new Date(lift.date._d);
      if(liftDate && liftDate.getFullYear()){
        let date1 = new Date();
        var date1_tomorrow = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate() + 1);
        if(date1.getFullYear() == liftDate.getFullYear() && date1.getMonth() == liftDate.getMonth() && date1.getDate() == liftDate.getDate()){
          ret = "Today";
        }
        else if (date1_tomorrow.getFullYear() == liftDate.getFullYear() && date1_tomorrow.getMonth() == liftDate.getMonth() && date1_tomorrow.getDate() == liftDate.getDate()) {
          ret = "Tomorrow"; // date2 is one day after date1.
        }
        else{
          ret = String(weekday[liftDate.getDay()]);
        }
      }
    }
    return(ret);
  }

  //the default way lifts are sorted (referenced in the sortBy: section of the ng-repeat for lifts)
  $scope.sortLiftBy = 'departureTime';

  //Not using this method yet, but may eventually want to use it to change the way
  // lifts are sorted. All you have to do is change the value of $scope.sortLiftBy
  // by passing in a different parameter (Ex: ng-click="changeSortingParam(driverRating)"
  $scope.changeSortingParam = function(sortBy) {
    $scope.sortLiftBy = sortBy;
  };

  $scope.disableTap = function(){
    container = document.getElementsByClassName('pac-container');
    // disable ionic data tab
    angular.element(container).attr('data-tap-disabled', 'true');
    // leave input field if google-address-entry is selected
    angular.element(container).on("click", function(){
        document.getElementById('pickupLocationInput').blur();
    });
  };

  /// Select Destination Modal ///

  $ionicModal.fromTemplateUrl('client/templates/selectDestination.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(selectDestinationModal) {
    $scope.selectDestinationModal = selectDestinationModal;
  });

    $scope.openSelectDestinationModal = function() {
        $scope.selectDestinationModal.show();
    };

    $scope.closeSelectDestinationModal = function() {
      $scope.selectDestinationModal.hide();
    };

    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.selectDestinationModal.remove();
    });

    // Execute action on hide modal
    $scope.$on('selectDestinationModal.hidden', function() {
      // Execute action
    });

    // Execute action on remove modal
    $scope.$on('selectDestinationModal.removed', function() {
      // Execute action
    });

    $scope.setDestination = function(locationObject){
      console.log(locationObject);
      //Set the resortValue property to the selected location ID
      $scope.selectedResort = locationObject;

      //close the modal
      $scope.closeSelectDestinationModal();
    };

    $scope.savePickupDropoffLocation = function(location){  //eventually pass in the location object here
      findLiftInputService.setPickupDropoffLocation(location);
    }

    $scope.autocompleteObjectPass = function(autocompletePlace){
        // console.log(" == autocompleteObjectPass == ");
        // console.log(autocompletePlace); //THIS IS THE AUTOCOMPLETE RETURN OBJECT

        let locationObject = {
          fullAddress: autocompletePlace.formatted_address,
          vicinity: autocompletePlace.vicinity,
          lat: autocompletePlace.geometry.location.lat(),
          long: autocompletePlace.geometry.location.lng()
        };

        $scope.pickupDropOffAddress = autocompletePlace.formatted_address;
        console.log("pickupDropOffAddress");
        console.log($scope.pickupDropOffAddress);

        $scope.savePickupDropoffLocation(locationObject);

    }

    $scope.toggleDirection = function(){
      if($scope.liftDirectionFilter == 'toDestination'){
        $scope.liftDirectionFilter = 'fromDestination';
      }
      else{
        $scope.liftDirectionFilter = 'toDestination';
      }
    }



}
