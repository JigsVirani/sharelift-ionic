angular
.module('Sharelift')
.controller('LiftViewCtrl', LiftViewCtrl);

function LiftViewCtrl ($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $rootScope, $location, $ionicActionSheet, findLiftInputService, liftStateService, uiGmapGoogleMapApi) {
  $reactive(this).attach($scope);
  var popupController = $controller('PopupCtrl', {$scope: $scope});


  //gives access to liftStateService variables and functions within HTML, use $root.stateService._____ to access.
  $rootScope.stateService = liftStateService;
  $rootScope.findLiftInputService = findLiftInputService;


  // Connects driver and rider specific controllers
  angular.expandControllerLiftViewDriver($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $location, $ionicActionSheet, popupController, liftStateService, uiGmapGoogleMapApi);
  angular.expandControllerLiftViewRider($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $location, $ionicActionSheet, popupController, liftStateService, uiGmapGoogleMapApi);


  //Subscriptions
  this.subscribe('getLocations');
  // this.subscribe('getLocations');
  this.subscribe('getUserData');
  this.subscribe('getLiftRatings');
  // this.subscribe('getRiderNotificationsConfirm');
  // this.subscribe('getRiderNotificationsDeny');
  this.subscribe('getAllRiderNotifications');
  this.subscribe('getPastLiftsDriver');
  this.subscribe('getPastLiftsRider');
  this.subscribe('getDriverNotifications');
  this.subscribe('allUserData');  //subscribes to all public data for all users - used for profile photos


  this.liftId = $stateParams.liftId;

  // Scope level variables
  $scope.requestLiftButtonDisabled = false;
  $scope.liftIsUnrequestable = false;
  $scope.isInThisLift = false;


  // Scope level STATE variables
  var thisLift = Lifts.findOne({_id: $stateParams.liftId});
  $scope.testLift = thisLift;
  //These variables are not reactive - they are assigned when this the lift view is loaded
  $scope.isActive = thisLift.isActive;
  $scope.driverEnRoute = thisLift.driverEnRoute;
  $scope.hasArrivedAtDestination = thisLift.hasArrivedAtDestination;
  $scope.isComplete = thisLift.isComplete;



  this.helpers({
    // the entire lift object - super useful...
    data() {
      return Lifts.findOne({_id : $stateParams.liftId});
    },

    // if the user is a confirmed rider in the lfit, returns the whole rider object.
    thisRider() {
      let ret;
      let lift = Lifts.findOne({_id : $stateParams.liftId});
      for(let i = 0; i < lift.confirmedRiders.length; i++){
        if(lift.confirmedRiders[i].userId == Meteor.userId()){
          ret = lift.confirmedRiders[i];
        }
      }
      return ret;

    },
    ratings() {
      return Ratings.find({liftId : $stateParams.liftId})
    },
  });


  // Truly have no idea if this is necessary. subHandleOneLift is never referenced anywhere.
  var subHandleOneLift = this.subscribe('getOneLift', () => {
    return [$stateParams.liftId];
    }, {
    onReady: function () {
      $scope.data = Lifts.findOne({_id : $stateParams.liftId});
      if(Lifts.find({_id : $stateParams.liftId}).count() === 0) {
        $state.go('Main.ride'); //redirect on invalid lift id
      }
    },
    onStop: function (error) {
      if (error) {

      } else {

      }
    }
  });

  // Do we need this either? I'll look into it. NR
  var subHandleLiftRatings = this.subscribe('getLiftRatings', () => {
    return [$stateParams.liftId];
    }, {
    onReady: function () {
      $scope.ratingsReady = true;
    },
    onStop: function (error) {
      if (error) {

      } else {

      }
    }
  });


  $scope.printOut = function(input){
    console.log(input);
  }


  $scope.getLiftDay = function(lift){
    var weekday = new Array(7);
    weekday[0]=  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    let ret;
    let liftDate;
    if(lift && lift.date){
      if(lift.date._d){
          liftDate = new Date(lift.date._d);
      }
      else{
          liftDate = new Date(lift.date);
      }
      if(liftDate && liftDate.getFullYear()){
        let date1 = new Date();
        var date1_tomorrow = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate() + 1);
        if(date1.getFullYear() == liftDate.getFullYear() && date1.getMonth() == liftDate.getMonth() && date1.getDate() == liftDate.getDate()){
          ret = "Today";
        }
        else if (date1_tomorrow.getFullYear() == liftDate.getFullYear() && date1_tomorrow.getMonth() == liftDate.getMonth() && date1_tomorrow.getDate() == liftDate.getDate()) {
          ret = "Tomorrow"; // date2 is one day after date1.
        }
        else{
          ret = String(weekday[liftDate.getDay()]);
        }
      }
    }
    return(ret);
  }

  $scope.getLiftMonthDay = function(lift){
    var month = new Array(12);
    month[0]=  "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    let ret;
    let liftDate;
    if(lift && lift.date){
      if(lift.date._d){
          liftDate = new Date(lift.date._d);
      }
      else{
          liftDate = new Date(lift.date);
      }
      ret = month[liftDate.getMonth()] + " " + liftDate.getDate();
    }
    return ret;
  }

  $scope.getUserPhone = function(id){
    let ret = "Phone not provided";
    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.phone){
        ret = String(user.profile.phone);
      }
    }
    return ret;
  }


  $scope.getProfilePicture = function(id){
    let ret = "/lift_profile.png";
    if(Meteor.users.findOne({_id:id})){ //FAILS HERE (I think because this controller isn't subscribing to just any user)
      let user = Meteor.users.findOne({_id:id});
      //console.log(user);
      if(user.profile.profilePicture){
        ret = user.profile.profilePicture; //Commented out for now, going to try to find a solution to this issue.
        //console.log(ret);
      }
    }

    return ret;
  }

  $scope.getDestinationName = function(destinationId){
    let ret = "the slopes";
    if(typeof(destinationId) == "number"){
      if(Locations.findOne({locationId:destinationId})){
          ret = Locations.findOne({locationId:destinationId}).destinationName;
      }
    }
    return ret;
  }

  //returns a full string with destination latitude and longitude. A silly way to do this (you should separate them, but yolo)
  // $scope.getDestinationGeoCoordinates = function(destinationId){
  //   let destinationLat = "Unknown";
  //   let destinationLong = "Unknown";
  //   if(Locations.findOne({locationId:destinationId})){
  //       location = Locations.findOne({locationId:destinationId});
  //       destinationLat = location.latitude;
  //       destinationLong = location.longitude;
  //   }
  //   // return destinationLat + " N, " + destinationLong + " W";
  //   return "getDestinationGeoCoordinates test works";
  // }
  //

  //THIS DOESN"T WORK RIGHT NOW
  // $scope.getGoogleMapsDestinationLink = function(destinationId){
  //   console.log(destinationId);
  //   let destinationLat = "Unknown";
  //   let destinationLong = "Unknown";
  //   if(typeof(destinationId) == "number"){
  //     if(Locations.findOne({locationId:destinationId})){
  //       console.log("Made it to 1");
  //         // location = Locations.findOne({locationId:destinationId});
  //         destinationLat = Locations.findOne({locationId:destinationId}).latitude;
  //         console.log(destinationLat);
  //         destinationLong = Locations.findOne({locationId:destinationId}).longitude;
  //         console.log(destinationLong);
  //     }
  //
  //   }
  //
  //   // return "https://maps.google.com/?q=" + destinationLat + "," + destinationLong;
  //   return "getGoogleMapsDestinationLink test worked";
  // }
  //
  //
  //


  $scope.getDriverVehicleInfo = function(id){
    let ret = "Vehicle not specified"
    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.vehicle){
          ret = String(user.profile.vehicle.year) + " " + user.profile.vehicle.make + " " + user.profile.vehicle.model;
      }
    }
    return ret;
  }

  $scope.getDriverVehiclePhoto = function(id){
    let ret = "/vehicle.png";

    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.vehicle.vehiclePicture){
          ret = user.profile.vehicle.vehiclePicture;
      }
    }
    return ret;
  }


  $scope.getDriverPhone = function(id){
    let ret = "Not provided";
    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.phone){
        ret =user.profile.phone;
      }
    }
    return ret;
  }


  $scope.liftHasEmptySeats = function(lift){
    let ret = false;
    if(lift){
        if(lift.confirmedRiders.length < lift.seats){
          ret = true;
        }
    }
    return ret;
  }

  $scope.liftHasPotentialRiders = function(lift){
    let ret = false;
    if(lift){
      if(lift.potentialRiders.length > 0){
        ret = true;
      }
    }
    return ret;
  }

  $scope.liftHasConfirmedRiders = function(lift){
    let ret = false;
    if(lift){
      if(lift.confirmedRiders.length > 0){
        ret = true;
      }
    }
    return ret;
  }

  $scope.liftHasNoRiders = function(lift){
    let ret = false;
    if(lift){
      if(lift.confirmedRiders.length === 0){
        ret = true;
      }
    }
    return ret;
  }


  // <<<< Rating Methods >>>> I'm not even sure that we are using these...

  $ionicModal.fromTemplateUrl('client/templates/rateUser.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.ratings = {
    rating1 : 3,
    rating2 : 3,
    rating3 : 3
  };

  $scope.leaveRatingRider = function(index) {
    $scope.ratingModalTitle = "Rate ya homies ya picked up";
    $scope.ratingType = "riderRating";
    $scope.curUser = $scope.lift.data.confirmedRiders[index].userId;
    $scope.modal.show();
  };

  $scope.leaveRatingDriver = function(index) {
    $scope.ratingModalTitle = "Rate yo driver sonnnn";
    $scope.ratingType = "driverRating";
    $scope.curUser = this.data.driverId;
    $scope.modal.show();
  };

  $scope.closeLeaveRating = function() {
    $scope.modal.hide();
  };

  $scope.submitRating = function() {


    var rating = {};
    if($scope.ratingType == "driverRating") {
      rating.riderId = Meteor.userId();
      rating.driverId = $scope.curUser;
    } else if($scope.ratingType == "riderRating") {
      rating.riderId = $scope.curUser;
      rating.driverId = Meteor.userId();
    }

    rating.rating1 = $scope.ratings.rating1;
    rating.rating2 = $scope.ratings.rating2;
    rating.rating3 = $scope.ratings.rating3;
    rating.liftId = this.data._id;
    rating.ratingType = $scope.ratingType;
    Meteor.call('submitRating', rating);
    $scope.ratings = {
      rating1 : 3,
      rating2 : 3,
      rating3 : 3
    };
    $scope.modal.hide();
  }


  $scope.isRateableRider = function(rider) { //according to the internet rateable and ratable are ok spellings
    if($scope.ratingsReady) {

      if(this.data.driverId == Meteor.userId()) {
        if(this.data && this.data.isComplete === false) {
          return false;
        }

        if(Ratings.find({driverId : Meteor.userId(), riderId : rider.userId, liftId : this.data._id}).count() == 0) {
          //if there are 0 ratings for this driver rating this rider, show button
          return true;
        }
      }
    } else {
      return false;
    }
  }

  $scope.isRateableDriver = function() { //can the rider rate this driver?
    if($scope.ratingsReady) {
      if(this.data.isComplete === false) {
        return false;
      }

      if(this.data.confirmedRiders.length) {
        for(var i = 0; i < this.data.confirmedRiders.length; ++i) {
          if(this.data.confirmedRiders[i].userId === Meteor.userId()) {
            //at this point, we have confirmed user was actually confirmed in the lift
            //and that we know all of the ratings in the db for the current lift
            if(Ratings.find({driverId : this.data.driverId, riderId : Meteor.userId(), liftId : this.data._id}).count() == 0){

              return true;
            }
          }
        }
      }
    }
    return false;
  }


  this.confirmRider = function(rider) {
    rider.liftId = $stateParams.liftId;
    Meteor.call('confirmRideRequest', rider);
  };

  this.denyRider = function(rider) {
    rider.liftId = $stateParams.liftId;
    Meteor.call('denyRideRequest', rider);
  };

  this.requestLift = function() {
    let self = this;
    function requestTheLift(){
      $scope.requestLiftButtonDisabled = true;
      request = {};
      request.liftId = self.data._id;
      request.userName = Meteor.user().services.facebook.name;
      request.user = Meteor.user();
      request.pickupDropoffLocation = findLiftInputService.getPickupDropoffLocation();
      request.pickupTime = "";
      request.driverId = self.data.driverId;
      request.driverName = self.data.driverName;
      request.phone = Meteor.user().profile.phone;


      Meteor.call('requestLift', request);

      $scope.requestLiftButtonDisabled = true;
    }

    if($scope.isLiftConflict()){
      popupController.showAlert("Oh snap!", "You're already confirmed in a lift for the same time, destination, and direction. You'll need to bail from that lift to request this one.", function(){})
    }
    else if(!Meteor.user().profile.phone){
      console.log("No phone!");
      popupController.showInputPopup("Oh snap!", "Please provide your phone number first!", function(number){
        console.log(number);
        if(number){
          let num = String(number).replace(/\D/g,''); // Regex to strip out non-numerical values
          console.log(num);
          if(num.length == 10){
            Meteor.call('addProfilePhoneNumber', Meteor.userId(), num);
            requestTheLift();
          }
          else{
              popupController.showConfirm("Oh snap!", "That wasn't a valid phone number. Please try it again.", function(confirm){
                if(confirm){
                    self.requestLift();
                }
              })
          }
        }
      });
    }
    else{
      requestTheLift();
    }
    //confirmation alert
    popupController.showAlert("Lift requested!", "The driver will be notified. Feel free to request some other lifts :)", function(){});
  };

  $scope.cancelRequest = function(lift){
    popupController.showConfirm("Woah there!", "Are you sure you want to cancel this request?", function(confirm){
      if(confirm){ //If they say OK,
        $location.path("/"); // redirect to home
        Meteor.call('cancelRideRequest', lift.data);// and cancel the lift request.
        $scope.requestLiftButtonDisabled = false;
      }
    }, "Yeah", "Nope");
  }

  $scope.liftHasBeenRequested = function() {
    //this function aint reactive tho
    if(this.data && this.data.potentialRiders && this.data.potentialRiders.length) {
      for(var i = 0; i < this.data.potentialRiders.length; ++i) {
        if(this.data.potentialRiders[i].userId === Meteor.userId()) {
          $scope.requestLiftButtonDisabled = true;
          return true;
        }
      }
    }
  }


//A go at the cancel button:
  $scope.cancelRider = function(liftData) {
    popupController.showConfirm("Woah there!", "Are you sure you want to leave this lift?", function(confirm){
      if(confirm){ // If they say OK,
        $location.path("/"); // redirect to home
        Meteor.call('cancelRider', liftData); // and cancel the lift
      }
    }, "Yeah", "Nope");
  };

  $scope.getCallRef = function(number){
    if(number){
      let num = String(number).replace(/\D/g,''); // Regex to strip out non-numerical values
      num = num.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"); // Super duper regex to put dashes back in
      num = "tel:+1-" + num;
      return num;

    }

  }

  $scope.isLiftConflict = function(){
    //use stateParams and a query on the current user to determine if the user
    // is already confirmed in a lift for the same time, destination, and direction. Or maybe just time... (since you just shouldn't be able to request lifts for the same time that you have a confirmed lift)
    return false;
  }

  ///////////////////////////////////////
}
