// RIDER-specific logic

angular.expandControllerLiftViewRider = function($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $location, $ionicActionSheet, popupController, liftStateService, uiGmapGoogleMapApi){


  //Warning: this is getting called like every second. Why is that? Should that be happening?
    $scope.isLiftUnrequestable = function(lift){
      // console.log(lift);
      let ret = false;
      $scope.liftIsUnrequestable = false;
      if(lift && lift.potentialRiders && lift.potentialRiders.length){
        for(let i = 0; i < lift.potentialRiders.length; i++){
          if(lift.potentialRiders[i].userId == Meteor.userId()){
            $scope.liftIsUnrequestable = true;
            ret == true;
          }
        }
      }
      return ret;
    };

    $scope.isLiftRequestable = function() {
      if(this.data) { //return false if the user is the driver or if not logged in
        if(this.data.driverId == Meteor.userId() || !Meteor.user()) {
          console.log("User is the driver");
          return false;
        }
        //return false if the number of confirmed riders is greater than or equal to the number of seats in the vehicle
        if(this.data.confirmedRiders && this.data.confirmedRiders.length >= this.data.seats){
          console.log("Lift is full");
          return false;
        }
        //return false if the lift is in the past
        if(this.data.departureTime < new Date()){
          console.log("Lift is in the past");
          return false;
        }
        //check if user is already
        var allRiders = [];
        allRiders.push.apply(allRiders, this.data.potentialRiders);
        allRiders.push.apply(allRiders, this.data.confirmedRiders);
        allRiders.push.apply(allRiders, this.data.deniedRiders);
        if(allRiders.length) {
          for(var i = 0; i < allRiders.length; ++i) {
            if(allRiders[i].userId === Meteor.userId()) {
              $scope.requestLiftButtonDisabled = true;
              return false;
            }
          }
        }
        return true;
      }
    }

    $scope.openContactDriverActionSheet = function(phoneNumber) {
      if(phoneNumber){
        let num = String(phoneNumber).replace(/\D/g,''); // Regex to strip out non-numerical values
        num = num.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"); // Super duper regex to put dashes back in
        let callHook = "tel:+1-" + num;
        let textHook = "sms:+1-" + num;

        // Show the action sheet:
        $ionicActionSheet.show({
          buttons: [{
            text: 'Call Driver'
          }, {
            text: 'Text Driver'
          }],
          cancelText: 'Cancel',
          buttonClicked: function(index, buttonObj) {
           switch (index) {
             case 0:
             //Call the number
              window.location.href = callHook;
               return false;
             case 1:
             //text the number
                 window.location.href = textHook;
               return false;
           }
          }
        });
      }
      else{
        alert("Something went wrong... the driver doesn't have a phone number on file.");
      }


   };


   $scope.getPickupTime = function (thisLift){
     let thisRider;
     for(let i = 0; i < thisLift.confirmedRiders.length; i++){
       if(thisLift.confirmedRiders[i].userId == Meteor.userId()){
         thisRider = thisLift.confirmedRiders[i];
       }
     }

     if(thisRider){
       let pickupTime = moment(thisLift.departureTime).add(thisRider.googleRouteInfo.originToPickupDropoff.duration.value, 'seconds').toDate();
       return pickupTime;
     }
     else{
       return null;
     }
   };


   $scope.getDropoffTime = function (thisLift){
     let thisRider;
     for(let i = 0; i < thisLift.confirmedRiders.length; i++){
       if(thisLift.confirmedRiders[i].userId == Meteor.userId()){
         thisRider = thisLift.confirmedRiders[i];
       }
     }

     if(thisRider){
       let dropoffTime = moment(thisLift.departureTime).add(thisRider.googleRouteInfo.originToPickupDropoff.duration.value, 'seconds').toDate();
       return dropoffTime;
     }
     else{
       return null;
     }
   };

   uiGmapGoogleMapApi.then(function(maps) {

            $scope.points = [
                {latitude: $scope.testLift.endLocation.lat,
                longitude: $scope.testLift.endLocation.long,
                idKey: 2,
                icon: 'Destination.png'
                }
            ]



            //Riders
            for(var i = 0; i < $scope.testLift.confirmedRiders.length; i++){
                console.log($scope.testLift)
              if($scope.testLift.confirmedRiders[i].userId == Meteor.userId()){
                $scope.points.push({
                    latitude: $scope.testLift.confirmedRiders[i].pickupDropoffLocation.lat,
                    longitude: $scope.testLift.confirmedRiders[i].pickupDropoffLocation.long,
                    idKey: 1,
                    icon: 'StartLocation.png'
                    })
              }
            }

            
            $scope.map = {center: {latitude: $scope.testLift.endLocation.lat, longitude: $scope.testLift.endLocation.long }, zoom: 8 };

            
            var styleArray = [
                    {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},
                    {"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},
                    {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},
                    {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},
                    {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
                    {"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}
                ]
            $scope.options = {
                scrollwheel: false,
                styles: styleArray,
                disableDefaultUI: true,
            };
            $scope.coordsUpdates = 0;
            $scope.dynamicMoveCtr = 0;
            $scope.marker = {
            id: 0,
            coords: {
                latitude: 4,
                longitude: 4
            },
            options: { draggable: false },
            events: {
                dragend: function (marker, eventName, args) {
                $log.log('marker dragend');
                if(!pos.coords){

                }
                var lat = marker.getPosition().lat();
                var lon = marker.getPosition().lng();
                $log.log(lat);
                $log.log(lon);

                $scope.marker.options = {
                    draggable: true,
                    labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
                    labelAnchor: "100 0",
                    labelClass: "marker-labels"
                };
                }
            }
            };
            $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
            if (_.isEqual(newVal, oldVal))
                return;
            $scope.coordsUpdates++;
            });
            $timeout(function () {
            $scope.marker.coords = {
                latitude: 42.1451,
                longitude: -100.6680
            };
            $scope.dynamicMoveCtr++;
            $timeout(function () {
                $scope.marker.coords = {
                latitude: 43.1451,
                longitude: -102.6680
                };
                $scope.dynamicMoveCtr++;
            }, 5000);
            }, 5000);


        });





}
