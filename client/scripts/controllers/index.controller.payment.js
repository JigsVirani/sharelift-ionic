angular.expandControllerIndexPayment = function($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $location, popupController){



  $scope.submitPayment = function(paymentForm, paymentModel) {
    console.log(" --- submitPayment called ---");
    console.log(paymentModel);

    //check if the form is valid

    //any alerts related to what was inputted

    //submit the payment via a Meteor method call
    Meteor.call('submitPayment', paymentModel, function(error, result){
      if(error){
        alert("error!");
      }
      else{
        if(result == false){  //if there is a problem, show an alert
          popupController.showAlert("Whoa there!", "We're not sure what's going on, but there is a problem...", function(){});
        }
        else{ //if nothing is wrong with the payment submission
          console.log(" // SUBMISSION SUCCESSFUL //");
          $scope.closePaymentModal();
          $scope.activeSlide = 1;
        }
      }
    });

  };

//Modal stuff
  $ionicModal.fromTemplateUrl('client/templates/paymentScreen.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(paymentModal) {
    $scope.paymentModal = paymentModal;
  });

  $scope.openPaymentModal = function(lift) {
    if(Meteor.users.findOne({_id:Meteor.userId()})){
      $scope.thisLift = lift;

      // the model that is modified in the paymentScreen.html, and passed into submitPayment()
      $scope.paymentModel = {
        fromUserId: Meteor.userId(),
        toUserId: lift.driverId,
        liftId: lift._id,
        paymentOption: "",  //for now, this means in what way the rider decided to pay: suggested, custom, or noPay
        amount: 7
      };

      $scope.paymentModal.show();
    }
  };

  $scope.closePaymentModal = function() {
    $scope.paymentModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.paymentModal.remove();
  });

  // Execute action on hide modal
  $scope.$on('paymentModal.hidden', function() {
    // Execute action
  });

  // Execute action on remove modal
  $scope.$on('paymentModal.removed', function() {
    // Execute action
  });








}
