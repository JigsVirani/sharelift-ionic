angular
  .module('Sharelift')
  .controller('LoginCtrl', LoginCtrl);

function LoginCtrl ($scope, $state, $meteor) {

  $scope.credentials = {
    // username: "",
    // password: ""
  };
  $scope.loginErrorMessage = "TEST";


  // --- LOGIN SCREEN ---

  $scope.fbLogin = function() {
    //Put facebook permissions in this:
    Meteor.loginWithFacebook({
        // loginStyle: "redirect",
        requestPermissions: ['email', 'user_friends', 'user_location', 'public_profile']
        }, function(err){
          if (err) {
                console.log(err);
              // throw new Meteor.Error("Facebook login failed");
          }
          else{
            console.log("Logged in!");

            Meteor.call('pullFBPicture');
            Meteor.call('pullFBFriendsData');
            console.log("FB ID should be set.........");
            console.log(Meteor.user());

            Meteor.call('notifyFriendsUsingSharelift');

            //check if the user has a phone number:
            if(Meteor.user().profile.phone){
              console.log("User already has a phone number. Go to main");
              //redirect to Main
              $state.go('Main');
              $scope.waiversCheck();
            }
            else{
              console.log("User needs to input a phone number. Go to phone input screen");
              //go to phoneInput screen
              $state.go('phoneInput');
            }



          }
      });
  }

  $scope.doLoginAction = function() { //non-facebook login

    $meteor.loginWithPassword($scope.credentials.username, $scope.credentials.password)
       .then(function() {
         console.log('Login success ');
         alert("logged in: " + $scope.credentials.username);
         //check if the user has a phone number:
         if(Meteor.user().profile.phone){
           console.log("User already has a phone number. Go to main");
           //redirect to Main
           $state.go('Main.ride');
           $scope.waiversCheck();
         }
         else{
           console.log("User needs to input a phone number. Go to phone input screen");
           //go to phoneInput screen
           $state.go('phoneInput');
         }

       }, function(_error) {
         console.log('Login error - ', _error);
        //  alert("Error: " + _error.reason);
         if(_error.reason == "Match failed"){
           console.log("match failed...");
           $scope.loginErrorMessage = "Your email and password seem... incompatible.";
         }
         else if(_error.reason == "User not found"){
           $scope.loginErrorMessage = "We don't have that email on file.";
         }
         else if(_error.reason == "Incorrect password"){
           $scope.loginErrorMessage = "Wrong password! Try again :)";
         }
         else{
           $scope.loginErrorMessage = "Something isn't right :/ ";
         }


        //  console.log('Login error - ', _error);
        //  if(_error.reason == "Username already exists"){
        //    $scope.createAccountError = "That username already exists";
        //  }
        //  else if(_error.reason == "Password may not be empty"){
        //    $scope.createAccountError = "You need a password!";
        //  }
        //  else{
        //    $scope.createAccountError = "Something isn't right :/ ";
        //  }
       });

     return false;
   }


$scope.goToForgotPassword = function(){
  console.log("goToForgotPassword called");
  $state.go('forgotPassword');  //navigate to the forgotPassword view
}

$scope.goToCreateAccount = function(){
  console.log("goToCreateAccount called");
  $state.go('createAccount');  //navigate to the forgotPassword view
}




// --- Forgot Password View ---

$scope.sendResetEmail = function(){
  let email = $scope.credentials.username;
  $scope.resetPassword(email);
}

$scope.resetPassword = function(emailAddress){
  console.log("resetPassword called");

  $meteor.forgotPassword({
    email: emailAddress
  })
  .then(function(_response) {
    console.log("Password reset email sent!");
    alert("A password reset email was sent to " + emailAddress);

  },
  function(_error) {
    console.log('Login error - ', _error);
    alert("Error: " + _error.reason);
  });

}

// --- Create Account View ---

$scope.createAccountError = "";

$scope.doCreateAccountAction = function() {
  console.log("-- CREATE ACCOUNT --");
  console.log($scope.credentials.username);
  console.log($scope.credentials.password);


  $meteor.createUser({
    username: $scope.credentials.username,
    email: $scope.credentials.username,
    password: $scope.credentials.password,
    profile: { createdOn: new Date() }
  })
  .then(function(_response) {
    console.log('doCreateAccountAction success');
    // alert("user created: " + $scope.credentials.username );

    console.log(Meteor.user());


    $state.go('addFBInfo');

  },
  function(_error) {
    console.log('Login error - ', _error);
    if(_error.reason == "Username already exists"){
      $scope.createAccountError = "That username already exists";
    }
    else if(_error.reason == "Password may not be empty"){
      $scope.createAccountError = "You need a password!";
    }
    else{
      $scope.createAccountError = "Something isn't right :/ ";
    }
  });
  return false;

}


  //gets added to the user's profile
  $scope.facebookData = {
    // firstName : "",
    // lastName : "",
    // gender : ""
  };

  $scope.submitFacebookData = function(){
    console.log("-- ADD FACEBOOK ACCOUNT INFO --");

    let data = {
      email : Meteor.user().username,
      firstName : $scope.facebookData.firstName,
      lastName : $scope.facebookData.lastName,
      fullName : $scope.facebookData.firstName + " " + $scope.facebookData.lastName,
      gender : "Male",
      locale : "en_US"
    }
    console.log("submitFacebookData called, with the following data:");
    console.log(data);
    Meteor.call('addFacebookInfoToUser', data);

    console.log("- Added FB data. Go to phone input -");
    $state.go('phoneInput');




  }



  $scope.waiversCheck = function(){
    //if the user hasn't signed both the Privacy Policy and the Terms & Conditions, open the waivers modal
    // let userNeedsToSign = false;
    if(Meteor.user().profile.hasAcceptedTermsConditions == true && Meteor.user().profile.hasAcceptedPrivacyPolicy == true){
      // do nothing - the user has signed both agreements
      console.log("do nothing - the user has signed both agreements");
    }
    else{
      console.log("Still needs to sign... opening modal");
      $scope.openWaiversModal(Meteor.user());
    }
  }


  // --- PHONE NUMBER input ---

  $scope.phoneModel = {
    phoneNumber : "",
    allowContact : true,
    allowContactText : "Yes",
    phoneErrorMessage : ""
  }

  $scope.toggleAllowContact = function(){
    if($scope.phoneModel.allowContact == true){
      $scope.phoneModel.allowContactText = "Yes";
    }
    if($scope.phoneModel.allowContact == false){
      $scope.phoneModel.allowContactText = "Nope";
    }
  }


  $scope.submitPhoneNumber = function(){
    console.log("-- SUBMIT PHONE --> phoneModel.phoneNumber is:");
    console.log($scope.phoneModel.phoneNumber);
    console.log($scope.phoneModel.allowContact);

    let num = String($scope.phoneModel.phoneNumber).replace(/\D/g,''); // Regex to strip out non-numerical values
    console.log(num);
    //if the number is 10 digits, insert the number:
    if(num.length == 10){
      console.log("adding number to profile");
      Meteor.call('addProfilePhoneNumber', Meteor.userId(), num);
      Meteor.call('updateCanContactUserField', $scope.phoneModel.allowContact);



      if(Meteor.user().profile.hasOnboarded == true){ //if the user has completed onboarding
        console.log(" - User has completed onboarding. Check for waiver, then go to main");
        $state.go('Main');
        $scope.waiversCheck();
      }
      else{ //hasOnboarded == false
        console.log(" - User has not completed onboarding. GO to preferences");
        $state.go('inputProfileSettings');
      }

    }
    else{
      console.log("not valid. Message is:");
      $scope.phoneModel.phoneErrorMessage = "Invalid phone number. Please provide a ten-digit number (area code plus seven digits).";
      console.log($scope.phoneModel.phoneErrorMessage);
    }

  }


  // input profile settings


  //gets added to the user's profile
  $scope.preferences = {
    // destinationId : "",
    // defaultRole : ""
  };

  $scope.submitProfileSettings = function(){
    console.log("submitProfileSettings called");
    console.log($scope.preferences);
    // Adds the preferences and sets “hasOnboarded” to true after
    Meteor.call('updateProfilePreferences', $scope.preferences);




    if($scope.preferences.defaultRole !== "rider"){  //the user is a driver - direct to driver signup
      console.log("Go to driver signup");
      $state.go('driverSignup');
      $scope.waiversCheck();
    }
    else{ //skip driver signup
      console.log("Skip driver sign up");
      $state.go('Main');
      $scope.waiversCheck();
    }



  }

}
