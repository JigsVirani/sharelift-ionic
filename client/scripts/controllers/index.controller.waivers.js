angular.expandControllerIndexWaivers = function($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $location, popupController){

  $scope.acceptPrivacyPolicy = false;
  $scope.acceptTermsConditions = false;
  $scope.errorMessage = "";

  $scope.toggleAcceptTermsConditions = function(){
    if($scope.acceptTermsConditions == false){
      $scope.acceptTermsConditions = true;
    }
    else{
      $scope.acceptTermsConditions = false;
    }
  }

  $scope.toggleAcceptPrivacyPolicy = function(){
    if($scope.acceptPrivacyPolicy == false){
      $scope.acceptPrivacyPolicy = true;
    }
    else{
      $scope.acceptPrivacyPolicy = false;
    }
  }




  $scope.submitWaiver = function() {
    console.log(" --- submitWaiver called ---");
    console.log($scope.acceptPrivacyPolicy);
    console.log($scope.acceptTermsConditions);


    if($scope.acceptPrivacyPolicy == false || $scope.acceptTermsConditions == false){ // if one of the checkboxes is left unchecked:
      $scope.errorMessage = "You must accept both policies to continue using ShareLift";
    }
    else{ //both checkboxes have been selected
      let privacyPolicyUpdateSuccessful = true;
      let termsConditionsUpdateSuccessful = true;

      Meteor.call('acceptPrivacyPolicy', function(error, result){
        if(error){
          alert("error!");
        }
        else{
          if(result == false){  //if there is a problem, show an alert
            privacyPolicyUpdateSuccessful = false;
            popupController.showAlert("Whoa there!", "We're not sure what's going on, but there is a problem...", function(){});
          }
          else{ //if nothing is wrong with the payment submission
            console.log(" // Privacy Policy Accepted //");
          }
        }
      });
      Meteor.call('acceptTermsConditions', function(error, result){
        if(error){
          alert("error!");
        }
        else{
          if(result == false){  //if there is a problem, show an alert
            termsConditionsUpdateSuccessful = false;
            popupController.showAlert("Whoa there!", "We're not sure what's going on, but there is a problem...", function(){});
          }
          else{ //if nothing is wrong with the payment submission
            console.log(" // Terms & Conditions Accepted //");
          }
        }
      });

      if(privacyPolicyUpdateSuccessful && termsConditionsUpdateSuccessful){
        console.log(" -- Pricacy Policy and Terms & Conditions accepted!");
        $scope.closeWaiversModal();
      }




    }


  };

//Modal stuff
  $ionicModal.fromTemplateUrl('client/templates/waivers.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(waiversModal) {
    $scope.waiversModal = waiversModal;
  });


  $scope.openWaiversModal = function(user) {
    console.log(" --  openWaiversModal called -- user is:");
    console.log(user);
    if(Meteor.user().profile.hasAcceptedTermsConditions == true){
      //the user has already accepted the most recent version of Terms & Conditions
      $scope.acceptTermsConditions = true;
    }
    if(Meteor.user().profile.hasAcceptedPrivacyPolicy == true){
      //the user has already accepted the most recent version of Privacy Policy
      $scope.acceptPrivacyPolicy = true;
    }

    $scope.waiversModal.show();
  };


  $scope.closeWaiversModal = function() {
    $scope.waiversModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.waiversModal.remove();
  });

  // Execute action on hide modal
  $scope.$on('waiversModal.hidden', function() {
    // Execute action
  });

  // Execute action on remove modal
  $scope.$on('waiversModal.removed', function() {
    // Execute action
  });








}
