angular
  .module('Sharelift')
  .controller('ProfileCtrl', ProfileCtrl);

function ProfileCtrl ($scope, $reactive, $stateParams, $meteor, $controller) {
  $reactive(this).attach($scope);
  var popupController = $controller('PopupCtrl', {$scope: $scope});
  this.subscribe('getLocations');
  this.subscribe('getUserData');
  this.subscribe('getLiftRatings');
  // this.subscribe('getRiderNotificationsConfirm');
  // this.subscribe('getRiderNotificationsDeny');
  this.subscribe('getAllRiderNotifications');
  this.subscribe('getPastLiftsDriver');
  this.subscribe('getPastLiftsRider');
  this.subscribe('getDriverNotifications');
  this.subscribe('allUserData');  //subscribes to all public data for all users - used for profile photos


  let userId = $stateParams.userId;


  // $meteor.subscribe('getPublicProfileData', userId).then(function(response){
  //   handler = response;
  //   $scope.profile = $meteor.collection(function() {
  //     return Meteor.users.findOne({_id : userId});
  //   });
  //   console.log($scope.profile);
  // });
  $scope.printOut = function(input){
    console.log(input);
  }
  $scope.getRatings = function(){
    if(Ratings.find({}).fetch()){
      console.log(Ratings.find({}).fetch())
    }
  }

  this.helpers({
    data() {
      let userId = $stateParams.userId;
      console.log(Meteor.users.findOne({_id : userId}));
      return Meteor.users.findOne({_id : userId});
    }
  });
}
