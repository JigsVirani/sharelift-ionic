angular.expandControllerMainProfile = function($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController, $cordovaCamera, photoUploadService){


//Edit Phone section
  $scope.editPhoneActive = false;

  let _number = "";


  $scope.takeProfilePhoto = function(){
    $meteor.getPicture().then(function(data){
      //console.log(data);
      Meteor.call('uploadProfilePhoto', data);
    });
  }

  // $scope.uploadProfilePhoto = function(){
  //     var options = {
  //       width: 800,
  //       height: 600,
  //       sourceType: Camera.PictureSourceType.PHOTOLIBRARY
  //     };
  //
  //   $meteor.getPicture().then(function(data){
  //     console.log(data);
  //     Meteor.call('uploadProfilePhoto', data);
  //   });
  // }


/// I WAS USING THESE TO TEST

  // $scope.uploadProfilePhoto1 = function(){
  //   var options = {
  //
  //               sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
  //               targetWidth: 500,
  //               targetHeight: 500
  //           };
  //
  //           $cordovaCamera.getPicture(options).then(function(imageData) {
  //             console.log(imageData);
  //             let imageSrc = "data:image/jpeg;base64," + imageData;
  //             console.log(imageSrc);
  //
  //             //Meteor.call('uploadProfilePhoto', imageData);
  //
  //               // var image = document.getElementById('myImage');
  //               // image.src = "data:image/jpeg;base64," + imageData;
  //               // $scope.newPost.imageData=image.src;
  //
  //           }, function(err) {
  //               // error
  //           });
  // }
  //
  // $scope.uploadProfilePhoto = function(){
  //   var options = {
  //               quality: 50,
  //               destinationType: Camera.DestinationType.DATA_URL,
  //               sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
  //               allowEdit: false,
  //               encodingType: Camera.EncodingType.PNG,
  //               targetWidth: 500,
  //               targetHeight: 500,
  //               popoverOptions: CameraPopoverOptions,
  //               saveToPhotoAlbum: false,
  //               correctOrientation:true
  //           };
  //
  //           $cordovaCamera.getPicture(options).then(function(imageData) {
  //             console.log(imageData);
  //             // let imageSrc = "data:image/jpeg;base64," + imageData;
  //             // console.log(imageSrc);
  //
  //             Meteor.call('uploadProfilePhoto', imageData);
  //
  //               // var image = document.getElementById('myImage');
  //               // image.src = "data:image/jpeg;base64," + imageData;
  //               // $scope.newPost.imageData=image.src;
  //
  //           }, function(err) {
  //               // error
  //           });
  // }
  // $scope.uploadProfilePhoto3 = function(){
  //   var options = {
  //               quality: 50,
  //               destinationType: Camera.DestinationType.FILE_URI,
  //               sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
  //               allowEdit: false,
  //               encodingType: Camera.EncodingType.JPEG,
  //               targetWidth: 500,
  //               targetHeight: 500,
  //               popoverOptions: CameraPopoverOptions,
  //               saveToPhotoAlbum: false,
  //               correctOrientation:true
  //           };
  //
  //           $cordovaCamera.getPicture(options).then(function(imageData) {
  //             console.log(imageData);
  //             let imageSrc = "data:image/jpeg;base64," + imageData;
  //             console.log(imageSrc);
  //
  //             //Meteor.call('uploadProfilePhoto', imageData);
  //
  //               // var image = document.getElementById('myImage');
  //               // image.src = "data:image/jpeg;base64," + imageData;
  //               // $scope.newPost.imageData=image.src;
  //
  //           }, function(err) {
  //               // error
  //           });
  // }

  $scope.takeVehiclePhoto = function(){
    photoUploadService.uploadVehiclePhoto();
  }



  $scope.updatePhoneInput = function(){
    let user = Meteor.users.findOne({_id:Meteor.userId()});
    if(user && user.profile && user.profile.phone){
      _number = user.profile.phone;
      $scope.phoneInput.number();
    }
  }

  $scope.phoneInput = {
    number: function(newNumber){
      return arguments.length ? (_number = newNumber) :  _number;
    }
  };


  $scope.editPhone = function(){
    $scope.editPhoneActive = true;
    setTimeout(function(){
        $("#edit-phone-text-field").focus();
    }, 100); //Heres a tiny delay to wait for the text field to become active. They we focus on it.
  }
  $scope.savePhone = function(){
    // console.log(_number);
    let num = String(_number).replace(/\D/g,''); // Regex to strip out non-numerical values
    // console.log(num);
    if(num.length == 10){
      $scope.editPhoneActive = false;
      Meteor.call('addProfilePhoneNumber', Meteor.userId(), _number);
    }
    else{
      popupController.showAlert("Oh snap!", "Hey, that's an invalid phone number. Please provide a ten-digit number (area code plus seven digits).", function(){
        setTimeout(function(){
          $("#edit-phone-text-field").focus();
        }, 500);

      });

    }

  }



  $scope.checkEditPhone = function(){
    if($scope.editPhoneActive){
      this.savePhone();
    }
    else{
      this.editPhone();
    }
  }

//Edit vehicle section
  let _make;
  let _model;
  let _year;
  let _seats = 4;

  $scope.editVehicleActive = false;

  $scope.vehicleInput = {
    make: function(newMake){
      return arguments.length ? (_make = newMake) :  _make;
    },
    model: function(newModel){
      return arguments.length ? (_model = newModel) :  _model;
    },
    year: function(newYear){
      return arguments.length ? (_year = newYear) :  _year;
    },
    seats: function(){
      return arguments.length ? (_seats = 4) : (_seats = 4);
    }
     //function(newSeats){ //Removed for now.
      //return arguments.length ? (_seats = newSeats) :  _seats; // Pulled this for now, we aren't doing seats right now.
    //}

  }

  $scope.checkSaveVehicle = function(){
    if(_make && _model && _year && _seats){
      this.saveVehicle();
    }
  }

  $scope.editVehicle = function(id){
    $scope.editVehicleActive = true;
    $scope.vehicleInput.seats();
    setTimeout(function(){
      $("#"+id).focus();
    },100); // Tiny delay here to wait for the UI to switch to edit mode
  }

  $scope.checkEditVehicle = function(id){
    if(id){
      this.editVehicle(id);
    }
    else{
      this.saveVehicle();
    }
  }

  $scope.saveVehicle = function(){
    $scope.editVehicleActive = false;
    let newVehicle = {}
    newVehicle.make = $scope.vehicleInput.make();
    newVehicle.model = $scope.vehicleInput.model();
    newVehicle.year = $scope.vehicleInput.year();
    newVehicle.seats = $scope.vehicleInput.seats();
    newVehicle.mpg = 25;
    Meteor.call('updateProfileVehicle', Meteor.userId(), newVehicle);
  }

  $scope.updateVehicleInput = function(){
    let user = Meteor.users.findOne({_id:Meteor.userId()});
    if(user && user.profile && user.profile.vehicle){
      _make = user.profile.vehicle.make;
      _model = user.profile.vehicle.model;
      _year = user.profile.vehicle.year;
      _seats = user.profile.vehicle.seats;
      $scope.vehicleInput.make();
      $scope.vehicleInput.model();
      $scope.vehicleInput.year();
      $scope.vehicleInput.seats();
    }
  }

}
