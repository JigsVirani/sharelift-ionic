angular.expandControllerMainSetters = function($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController, liftStateService){

///////////////////////////
// Navigation functions
//////////////////////////

// nothing here for now... Deleted the one method here on Nov. 2 2016

//////////////////////////
// Rating functions
//////////////////////////

  $scope.rateRider = function(rider, rating){
    rider.rating = rating;
  }
  $scope.rateDriver = function(lift, rating){
    lift.driverRating = rating;
  }

  // This is a big function that submits all the ratings, and works for both riders and drivers.
  // PLEASE OBSERVE that this function also resets or clears the lift.
  $scope.submitRatings = function(lift, type){
    if(type == "driverRating" && lift.driverRating){
      if(!lift.driverRating){
        lift.driverRating = 0;
      }
      let rating = {};
      rating.ratingType = type;
      rating.liftId = lift._id;
      rating.driverId = lift.driverId;
      rating.riderId = Meteor.userId();
      rating.rating1 = parseInt(lift.driverRating);
      rating.rating2 = parseInt(lift.driverRating);
      rating.rating3 = parseInt(lift.driverRating);
      Meteor.call('submitRating', rating);
    }
    else if(type == 'riderRating'){
      for(var x = 0; x<lift.confirmedRiders.length; x++){
        let rider = lift.confirmedRiders[x];
        if(!rider.rating){
          rider.rating = 0;
        }
        let rating = {};
        rating.ratingType = type;
        rating.liftId = lift._id;
        rating.driverId = lift.driverId;
        rating.riderId = rider.userId;
        rating.rating1 = parseInt(rider.rating);
        rating.rating2 = parseInt(rider.rating);
        rating.rating3 = parseInt(rider.rating);
        Meteor.call('submitRating', rating);
      }
    }
    // LOGIC NEEDED: If you're keeping this submitRatings function, you'll need to make sure it a) creates new rating objects and b) passes them into the lift.
  }


//////////////////////////////
// Notification functions
/////////////////////////////

  // Mark a notification as read
  $scope.markNotifRead = function(notif) {
    Meteor.call('markNotifRead', notif._id);
  }

 // Dismiss a notification. This hides it typically.
  $scope.dismissNotif = function(notif){
    console.log(notif);
    Meteor.call('dismissNotif', notif._id);
  }
}
