angular.expandControllerMainGetters = function($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController, liftStateService){

  $scope.getDestinationByLift = function(lift){
    let ret = "the slopes"
    if(lift && lift.destinationLocationId){
      if(Locations.findOne({locationId:lift.destinationLocationId})){
        ret = Locations.findOne({locationId:lift.destinationLocationId}).destinationName;
      }
    }
    return ret;
  }

  $scope.getRidersIsPlural = function(lift){
    let ret = "";
    if(lift && lift.confirmedRiders){
      if(lift.confirmedRiders.length > 1){
        ret = "s";
      }
    }
    return ret;
  }

  $scope.getUserRatings = function(id){
    let ret = "";
    if(Meteor.users.findOne({_id:id})){
      console.log(Ratings.find({}).fetch());
      let driverRatings = Ratings.find({ratingType:'driverRating'}).fetch();
      let riderRatings = Ratings.find({ratingType:'riderRating'}).fetch();
      console.log(driverRatings);
      console.log(riderRatings);
    }
    return ret;
  }

  $scope.getDriverPhoto = function(lift){
    let ret = "/lift_profile.png";
    if(lift && lift.driverId){
      let driver = Meteor.users.findOne({_id : lift.driverId});
      if(driver){
        if(driver.profile.profilePicture){
          ret = driver.profile.profilePicture;
        }
      }
    }
    return ret;
  }

  $scope.getDriverPhoto2 = function(profile){
    let ret = "";
    if(profile && profile.services && profile.services.facebook){
      console.log(profile.services.facebook.id);
      ret = "http://graph.facebook.com/" + profile.services.facebook.id + "/picture/?type=large"
      console.log(ret);
    }
    return ret;
  }
  $scope.getRiderPhoto = function(rider){
    let ret = "/lift_profile.png";
    if(rider && Meteor.users.findOne({_id:rider.userId})){
      rider = Meteor.users.findOne({_id:rider.userId});
      ret = rider.profile.profilePicture;
    }
    return ret;
  }
  $scope.getMyPickupTime = function(lift){
    let ret = "";
    if(lift && lift.confirmedRiders){
      let riders = lift.confirmedRiders;
      let user;
      for(var x = 0; x<riders.length; x++){
        if(riders[x].userId == Meteor.userId()){
          user = riders[x];
        }
      }
      if(user){
        ret = user.pickupTime;
      }
    }
    return ret;
  }


  $scope.getDriverVehicleInfo = function(id){
    let ret = "Vehicle not specified"
    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.vehicle.year !== -1){
        ret = String(user.profile.vehicle.year) + " " + user.profile.vehicle.make + " " + user.profile.vehicle.model;
      }
    }
    return ret;
  }
//deleted getLocations function from here


  $scope.getSeatsAvailable = function(lift){
    let ret;
    if(lift){
      let openSeats = parseInt(lift.seats) - lift.confirmedRiders.length
      ret = String(openSeats) + " of " + String(lift.seats);
    }
    return ret;
  }

  $scope.getRouteOverview = function(lift){
    // console.log(" == getRouteOverview CALLED! ==");
    // console.log(lift);
    // console.log(lift.destinationLocationId);
    let thisLocation = Locations.findOne({locationId: lift.destinationLocationId});
    let city = thisLocation.city;


    // console.log(city);

    let destination = $scope.getDestinationByLift(lift);
    var fromLocation;
    var toLocation;

    if(lift.liftDirection == 'toDestination'){
      fromLocation = city;
      toLocation = destination;
    }
    else{
      fromLocation = destination;
      toLocation = city;
    }

    return fromLocation + " to " + toLocation;
  };

  $scope.getConfirmationStatus = function(lift){
    let ret = "Available for reqest"
    if(lift && lift.potentialRiders.length){
      for(let i = 0; i< lift.potentialRiders.length;i++){
        if(lift.potentialRiders[i].userId == Meteor.userId()){
          ret = "Request pending";
        }
      }
    }
    if(lift.departureTime < new Date()){
      ret = "Already departed!";
    }
    return ret;
  };

  $scope.getConfirmationStatusClass = function(lift){
    let ret = "";
    if(lift && lift.potentialRiders.length){
      for(let i = 0; i< lift.potentialRiders.length;i++){
        if(lift.potentialRiders[i].userId == Meteor.userId()){
          ret = "list-confirmation-pending";
        }
      }
    }
    return ret;
  };

  $scope.userHasPhoneNumber = function(id){
    let ret = false;
    if(Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.phone){
        ret = true;
      }
    }
    return ret;
  }
  $scope.userHasVehicleMake = function(id){
    let ret = false;
    if(Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.vehicle.make){
        ret = true;
      }
    }
    return ret;
  }

  $scope.userHasVehicleModel = function(id){
    let ret = false;
    if(Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.vehicle.model){
        ret = true;
      }
    }
    return ret;
  }

  $scope.userHasVehicleYear = function(id){
    let ret = false;
    if(Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.vehicle.year){
        ret = true;
      }
    }
    return ret;
  }

  $scope.userHasVehicleSeats = function(id){
    let ret = false;
    if(Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.vehicle.seats){
        ret = true;
      }
    }
    return ret;
  }

  function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}


// --- Notification / Alerts Page --- //

  $scope.getNotifTimeElapsed = function(notif){
    return timeSince(notif.date);
  }

  $scope.getNotifIcon = function(notif){
    let ret = "ion-android-car";
    switch(notif.notifType){
      case "denyLift":
        ret = "ion-close-circled";
        break;
      case "confirmLift" :
        ret = "ion-checkmark-circled";
        break;
      case "requestLift":
        ret = "ion-plus-circled";
        break;
      case "driverOnTheWay":
        ret = "ion-alert";
        break;
      case "driverIsHere":
        ret = "ion-alert";
        break;
      default:
        ret = "ion-close-circled";
    }
    return ret;
  }

  $scope.getNotifTextStyle = function(notif){
    if(notif.read == true){
      return "notification-text-read";
    }
    else{
      return "notification-text-undread";
    }
  }

//determines which URL to route to from a notification
  $scope.getLiftUrl = function(notif){
    var url;
    if(notif.driverId == Meteor.userId()){ //user is the driver
      url = "#/driverlift/" + notif.liftId;
      return url;
    }
    else{ //user is not the driver
      url = "#/lift/" + notif.liftId;
      return url;
    }
  };


//THIS FUNCTION ASSUMES A 60 MIN TRAVEL TIME. Modify in the future.
  $scope.hasLiftRequestsForThisTime = function(liftDepartureTime){
    let ret = false;

    //ASSUMING 60 minutes travel time for now
    var liftArrivalTime = moment(liftDepartureTime).add(60, 'minutes').toDate();
    // find all lifts user has requested
    let potentialLifts = Lifts.find({"potentialRiders.userId": Meteor.userId(), isComplete: false}).fetch();

    for(let i = 0; i < potentialLifts.length; i++){
      var iArrivalTime = moment(potentialLifts[i].departureTime).add(potentialLifts[i].estimatedTravelTime, 'minutes').toDate();

      //if a given lift overlaps with the lift that is being offered, return true.
      if(liftArrivalTime >= potentialLifts[i].departureTime && liftDepartureTime <= iArrivalTime){
        ret = true;
      }
      else{ //Otherwise return false.
        ret = false;
      }
    }
    return ret;
  }
}
